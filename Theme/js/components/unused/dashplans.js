App.dashplans = (function($) {
    "use strict";
    var _Initialised = false;


    var wasViewport;

    var currentViewport;

    var _Carousel;

    var $labels;


    function init() {
        if (!_Initialised && $(".dashplans").length) {
            _Initialised = true;

            _Carousel = $(".dashplans-list");

            initCarousel();

            handleResize();

            // initEvents();


        }
    }

    function initEvents() {






      // initPaymentFrequency();


          // handleResize();

            // $(window).on("debouncedresize", function() {

            //     App.dashchangeplans.handleResize();

            // })


    }



   //  function initPaymentFrequency(){

   //  $("[name=plan]").on("change",function(){


   //    if($("[name=plan]:checked").val()=="monthly"){

   //      $(".formsection-clarification--monthly").stop().slideDown();
   //      $(".formsection-clarification--annually").stop().slideUp();

   //    }else{

   //      $(".formsection-clarification--monthly").stop().slideUp();
   //      $(".formsection-clarification--annually").stop().slideDown();

   //    }



   // });


   //  }


// function handleResize(){



// }


  function initCarousel(){



   _Carousel.slick({
      mobileFirst: true,
      draggable:true,
      dots: false,
      infinite: false,
      speed: 300,
      arrows:true,
      adaptiveHeight: true,

      centerMode: true,
      centerPadding: '17%',
  variableWidth: false,
      focusOnSelect: true,

      slidesToShow: 1,
      slidesToScroll: 1,

      responsive: [
        {
          breakpoint: 767,
          settings: {
          variableWidth: true,
          centerMode: false,
          centerPadding: '0%',
            slidesToShow: 4,
            slidesToScroll: 4,
          }
        }
      ]
    });



      _Carousel.on('beforeResize', function(event, slick, direction){

        handleResize();

      });

      _Carousel.on('init reInit', function(event, slick, direction){

        resizeRows()

      });





  }


    function handleResize(){


        if(viewport().width<768){
          currentViewport = "mobile";
        }else{
          currentViewport = "tablet";
        }
          if(wasViewport!=currentViewport){

            if(currentViewport=="mobile"){
               $labels = $(".dashplans-item.is-labels");
               _Carousel.slick("slickRemove",0)



            }else{
               // $labels = $(".dashplans-list").detach();
               _Carousel.slick("slickAdd",$labels, 0, true)

            }

          }

          wasViewport = currentViewport;




    }


function resizeRows(){


        var $labels = $(".dashplans-item:nth-child(1) .dashplans-row");
        var $rows = $(".dashplans-row");
        $rows.height("auto");
        var currentHeight;



        for (var row = 0; row < $labels.length; row++) {

            var minHeight = 0;

            for (var column = 0; column < 3; column++) {


                currentHeight = $(".dashplans-item:eq(" + column + ") .dashplans-row:eq(" + row + ")").height()
                if (currentHeight > minHeight) {
                    minHeight = currentHeight;
                }



            };

            $(".dashplans-item .dashplans-row:nth-child(" + (row + 1) + ")").height(minHeight)


        };

}


    return {
        init: init
    }
}(window.jQuery));
