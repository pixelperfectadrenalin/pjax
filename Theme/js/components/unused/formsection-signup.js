App.formsectionSignup = (function($) {
  "use strict";
  var _Initialised = false;
  var didValidate = false;


  function init() {
    if (!_Initialised && $(".formsection--signup").length) {
      _Initialised = true;
      initForm();


    // $(".formsection--signup").waitForImages(function() {
    //     App.formsectionSignup.equalHeight();
    // });

    }
  }


   function initForm() {




      $("#dateofbirth").on("click focus touchstart",function(){


          if($("#dateofbirth").val()==""){

            var birthDate = new Date();
            birthDate.setFullYear(birthDate.getFullYear() - 40);




            var day = ("0" + birthDate.getDate()).slice(-2);
            var month = ("0" + (birthDate.getMonth() + 1)).slice(-2);

            var dateString = birthDate.getFullYear()+"-"+(month)+"-"+(day);

            $("#dateofbirth").val(dateString);



          }


      });



   $(".form-row--agreetodirectdebitterms").hide();


   $(".form-row--yourmovingindate").hide();

   // $(".form-row--concessionnumber").hide();

   $(".formsection-creditcardordirectdebit-directdebit").hide();

   $(".formsection-passwordinstructions").hide();


    $("[name=movinghome]").on("change",function(){


      if($("[name=movinghome]:checked").val()=="yes"){
        $(".form-row--yourmovingindate").stop().slideDown();
      }else{
        $(".form-row--yourmovingindate").stop().slideUp();
      }

    });



    $("[name=paymentoption]").on("change",function(){


      if($("[name=paymentoption]:checked").val()=="directdebit"){
        $(".form-row--agreetodirectdebitterms").stop().slideDown();
      }else{
        $(".form-row--agreetodirectdebitterms").stop().slideUp();
      }

    });





    $("[name=plan]").on("change",function(){


      if($("[name=plan]:checked").val()=="monthly"){

        $(".formsection-clarification--monthly").stop().slideDown();
        $(".formsection-clarification--annually").stop().slideUp();

      }else{

        $(".formsection-clarification--monthly").stop().slideUp();
        $(".formsection-clarification--annually").stop().slideDown();

      }


    });




    $("#password, #confirmpassword").on("focus",function(){

        $(".formsection-passwordinstructions").stop().slideDown();


    });





    $("[name=paymentoption]").on("change",function(){


      if($("[name=paymentoption]:checked").val()=="creditcard"){
        $(".formsection-creditcardordirectdebit-creditcard").stop().slideDown();
        $(".formsection-creditcardordirectdebit-directdebit").stop().slideUp();
      }else{
        $(".formsection-creditcardordirectdebit-directdebit").stop().slideDown();
        $(".formsection-creditcardordirectdebit-creditcard").stop().slideUp();
      }


   });



  }







  return {
    init: init
  }
}(window.jQuery));
