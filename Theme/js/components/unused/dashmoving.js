App.dashmoving = (function($) {
    "use strict";
    var _Initialised = false;


    function init() {
        if (!_Initialised && $(".dashmoving").length) {
            _Initialised = true;
            initEvents();


        }
    }




    function initEvents() {


        $(".formsection-creditcardordirectdebit-directdebit").hide();


        initPlanToggle();


        initPaymentMethod();

        initPaymentOption();


    }



function initPaymentMethod(){

        $("[name=paymentmethod]").on("change",function(){


      if($("[name=paymentmethod]").val()=="add"){

        $(".dashmoving-addpayment").stop().slideDown();

      }else{

        $(".dashmoving-addpayment").stop().slideUp();

      }


    });

    }


function initPaymentOption(){
    $("[name=paymentoption]").on("change",function(){


      if($("[name=paymentoption]:checked").val()=="creditcard"){
        $(".formsection-creditcardordirectdebit-creditcard").stop().slideDown();
        $(".formsection-creditcardordirectdebit-directdebit").stop().slideUp();
      }else{
        $(".formsection-creditcardordirectdebit-directdebit").stop().slideDown();
        $(".formsection-creditcardordirectdebit-creditcard").stop().slideUp();
      }


   });

}



    // function initForm(){

    //   $(".dashmoving-form--step1").submit(function(){

    //     if($(this).valid()){

    //       $(this)[0].submit();

    //     }

    //   })

    //   }


function initPlanToggle(){

        $("[name=plan]").on("change",function(){


      if($("[name=plan]:checked").val()=="monthly"){

        $(".formsection-clarification--monthly").stop().slideDown();
        $(".formsection-clarification--annually").stop().slideUp();

      }else{

        $(".formsection-clarification--monthly").stop().slideUp();
        $(".formsection-clarification--annually").stop().slideDown();

      }


    });




}


    return {
        init: init
    }
}(window.jQuery));
