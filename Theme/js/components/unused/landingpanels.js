App.featureoverview = (function($) {
  "use strict";
  var _Initialised = false;
  var _hasPlayed = false;

  function init() {
    if (!_Initialised && $(".featureoverview-list").length) {
      _Initialised = true;
      initEvents();
    }
  }

  function initEvents() {


            equalHeight();

            $(window).on("debouncedresize", function() {
              App.featureoverview.equalHeight();
            })

            $(window).on("load", function() {
              App.featureoverview.equalHeight();
            })




  }


  function equalHeight(){
    App.equalHeight.equalHeight({elements:".featureoverview-item-content-text-copy"});
    //setTimeout(function() {  App.equalHeight.equalHeight({elements:".featureoverview-imageandtext-text-copy"}); }, 150);
   

  }


  return {
    init: init,
    equalHeight: equalHeight
  }
}(window.jQuery));
