App.popupgeneralcontent = (function($) {
  "use strict";
  var _Initialised = false;

  var _hasPlayed = false;

  var isPopupOpen = false;




  function init() {
    if (!_Initialised && $(".popup-generalcontent").length) {
      _Initialised = true;
      initEvents();
    }
  }


  function initEvents() {



    $("body").on("click",".popup-popupgeneralcontent-trigger", function(e){

      e.preventDefault();

      openPopup($($(this).attr("href"))[0], this)

    });


  }




// function openPopupFromInternalLink(URL){


//         $.magnificPopup.open({


//             items: {
//                 src: URL,
//                 type: "ajax"
//             }

//           });


//         }



    function openPopup(element, callerElement, onOpen, onClose){



        var popupType;

        var popupCallback;


              if(typeof element==="string"){

                popupType = "ajax";

              }else{

                popupType = "inline";

              }



                $.magnificPopup.open({

                    // prependTo: $("form:first"),
                    // delegate: 'a',
                    items: {
                        src: element,
                        type: popupType
                    },
                    // tLoading: 'Loading image #%curr%...',
                    mainClass: 'popup-eneralcontent-outer mfp-zoomin',


                    closeBtnInside: true,
                    //preloader: false,
                    removalDelay: 400, // Delay to allow for fade out

                    preloader: true,

                    callbacks: {


                      buildControls: function() {
                        // re-appends controls inside the main container
                        // this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                      }

                      ,

                      ajaxContentAdded: function() {


                        if(popupType == "ajax" & !isPopupOpen){
                        isPopupOpen = true;

                        App.popupzoom.onMagnificOpen(callerElement);

                        if(typeof onOpen == "function"){
                          onOpen()
                        }

                        }



                      },
                      open: function() {

                        console.log("isPopupOpen",isPopupOpen)

                        if(popupType == "inline" & !isPopupOpen){
                        isPopupOpen = true;


                        App.popupzoom.onMagnificOpen(callerElement);

                        if(typeof onOpen == "function"){
                          onOpen()
                        }

                        }



                      },

                      close: function() {
                        isPopupOpen = false;

                        // Will fire when popup is closed
                        if(typeof onClose == "function"){
                          onClose()
                        }

                        // $(".js-popup-dash-close").off("click.popupdash");

                      },
                      beforeClose:function(){

                        App.popupzoom.onMagnificClose(callerElement);

                      },

                      // close: function() {
                      //   // console.log('Popup removal initiated (after removalDelay timer finished)');
                      // },
                      // afterClose: function() {
                      //   // console.log('Popup is completely closed');
                      // }
                    }

                });

}



  return {
    init: init
  }
}(window.jQuery));
