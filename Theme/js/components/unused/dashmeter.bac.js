App.dashmeter = (function($) {
    "use strict";
    var _Initialised = false;
    var currentPeriod;
    var previousPeriod;
    var nextPeriod;
    var chartData;
    var maxValue;
    var minValue;
    var topValue;
    var range;
    var exponent;
    var magnitude;
    var value = 0;
    var valueCaption;
    var interval;
    var axisIncrementValue;
    var $labelSet;
    var labelTemplate;
    var valueTemplate;
    var valueLabel;
    var $scrollAnimationDiv;
    var chartTypes = ["spend", "usage"];
    var isSmoothInit = false;
    var newScrollState;
    var oldScrollState;

    var selectValueIndex = 0;

    var spendOrUsage = "spend";


    var ajaxData;


    function init() {
        if (!_Initialised && $(".dashmeter").length) {
            _Initialised = true;
            $scrollAnimationDiv = $("<div>");

            FastClick.attach($(".dashmeter-chart-chart")[0]);
            initEvents();
        }
    }



    function initEvents() {
        initChart();
        if (!Modernizr.touchevents) {

            $('.dashchart-content:first').smoothWheel({
                // onRender: onScrollMove
            });
            isSmoothInit = true;

            $('.dashchart-content').kinetic({
                moved: function() {
                        // console.log("moved",isSmoothInit)
                    if (isSmoothInit) {
                        $('.dashchart-content:first').smoothWheel({
                            remove: true
                        });
                        isSmoothInit = false;
                    }
                },
                stopped: function() {
                        // console.log("stopped",isSmoothInit)
                    if (!isSmoothInit) {
                        $('.dashchart-content:first').smoothWheel({
                            // onRender: onScrollMove
                        });
                        isSmoothInit = true;
                    }
                }
            });




            // $('.dashchart-content').kinetic();
        }
    }



    // function onScrollMove(vx){


    //  if(vx >= 1){

    //     newScrollState = "moving";

    //  }else{

    //     newScrollState = "stopped";

    //  }

    //  if(oldScrollState != newScrollState){

    //     console.log("onscrollstate",newScrollState)

    //     if(newScrollState == "stopped"){

    //         $('.dashchart-content').kinetic({
    //             onRender: onScrollMove
    //         })

    //     }
    //     if(newScrollState == "moving"){

    //         $('.dashchart-content').kinetic('stop')

    //     }
    //  }

    //  oldScrollState = newScrollState

    // }


    function initChart() {
        loadChartData("now");

        console.log($(".dashchart-values-item"))


        $(".dashmeter-intro-lastupdated-icon").on("click", function() {
            loadChartData(currentPeriod);
        })
        $("#dashmeter-spendvsusage").on("change", function() {
            // loadChartData(currentPeriod);
            spendOrUsage = $("#dashmeter-spendvsusage").prop("checked") ? "spend" : "usage";
            renderChart()
        })
        $(".dashmeter-datetoggle-date-prev").on("click", function() {
            loadChartData(previousPeriod);
        })
        $(".dashmeter-datetoggle-date-next").on("click", function() {
            loadChartData(nextPeriod);
        })


        $(document).on("click", ".dashchart-values-item", function() {

            $(".dashchart-values-item").not(this).removeClass("is-active");
            $(this).addClass("is-active");


            chartData = ajaxData.data[spendOrUsage];

            selectValueIndex = $(this).index();

            var theValue = ajaxData.data[spendOrUsage].values[selectValueIndex]








            $(".is-smartmeter .dashmeter-chart-info-data-item-value").text(chartData.valuePattern.replace("[X]",theValue.positive.toFixed(2)))
            $(".is-solarsavings .dashmeter-chart-info-data-item-value").text(chartData.valuePattern.replace("[X]",theValue.negative.toFixed(2)))

            $(".dashmeter-chart-info-date-value").text(theValue.longlabel)

            $(".dashmeter-chart-info-link a").attr("href", theValue.longlabel)

            $(".dashmeter-chart-info-link a").attr("href")

            $(".dashmeter-chart-info-link a").attr("href",$(".dashmeter-chart-info-link a").attr("href-src").replace("[X]",theValue.ID))



        });

    }

    function loadChartData(period) {
            $.ajax({
                dataType: "json",
                url: $("#metercharturlstart").val() + $("#metercharttype").val() + $("#metercharturlmiddle").val() + period + $("#metercharturlend").val(),
                error: function(data) {
                    console.log("Error", data)
                },
                success: function(data) {

                    ajaxData = data;

                    renderChart();

                }
            })
        }





        // function startAnimations(){
        //             // $(".dashmeter-chart").addClass("is-animations-enabled");
        //             window.requestAnimationFrame(function(){
        //                 $(".dashmeter-chart").addClass("is-animations-animate");
        //             })
        // }



function renderChart(){
        var data = ajaxData;




            $(".dashmeter-chart").toggleClass("has-negative",data.hasSolar );




            $(".dashmeter-chart-info-data-item.is-solarsavings").toggle(data.hasSolar)

            $(".dashchart-values-item-negative").toggle(data.hasSolar)

            $(".dashchart-labels-set.is-negative").toggle(data.hasSolar);




                    previousPeriod = data.previousPeriod.id;
                    nextPeriod = data.nextPeriod.id;
                    currentPeriod = data.currentPeriod.id;
                    $(".dashmeter-datetoggle-date-current").text(data.currentPeriod.label)
                    $(".dashmeter-datetoggle-date-prev").attr("title", data.previousPeriod.label)
                    $(".dashmeter-datetoggle-date-next").attr("title", data.nextPeriod.label)
                    $(".dashchart-values-list").empty();
                    $(".dashchart-labels-set-list").empty();
                    // $(".dashmeter-chart").removeClass("is-animations-enabled");
                    // // $(".dashmeter-chart").removeClass("is-animations-enabled");
                    // // $(".dashmeter-chart").addClass("is-animations-animate");
                    // $(".dashmeter-chart").removeClass("is-animations-animate");

                    // window.requestAnimationFrame(function() {
                            $(".dashmeter-chart").removeClass("is-animations-enabled");
                            $(".dashmeter-chart").removeClass("is-animations-animate");
                        // })



                    // for (var x = 0; x < chartTypes.length; x++) {


                        chartData = data.data[spendOrUsage];
                        // data.hasSolar = (chartTypes[x] == "spend");
                        $(".is-positive .dashchart-labels-set-caption").text(chartData.positiveLabel)
                        $(".is-negative .dashchart-labels-set-caption").text(chartData.negativeLabel)
                        $(".dashchart-labels-unit").text(chartData.unit)
                        $(".dashchart-labels-unit").text(chartData.unit).toggleClass("is-using-systemfont", chartData.unit == "$" || chartData.unit == "¢")


                        $(".is-smartmeter .dashmeter-chart-info-data-item-label").text(chartData.positiveToolLabel)
                        $(".is-solarsavings .dashmeter-chart-info-data-item-label").text(chartData.negativeToolLabel)


                        maxValue = 0;
                        minValue = 9999999999999;
                        for (var i = 0; i < chartData.values.length; i++) {
                            value = chartData.values[i];
                            maxValue = Math.max(maxValue, value.positive);
                            minValue = Math.min(minValue, value.positive);
                            if (data.hasSolar) {
                                maxValue = Math.max(maxValue, value.negative);
                                minValue = Math.min(minValue, value.negative);
                            }
                        };







var noTicks = 5





            // intervalType = "number";

            range = getNiceNumber(maxValue - minValue, false);

            var axisIncrement = getNiceNumber(range / (noTicks - 1), true);


                var minimum = Math.floor(minValue / axisIncrement) * axisIncrement;

                var maximum = Math.ceil(maxValue / axisIncrement) * axisIncrement;







console.log("minimum",minimum)
console.log("maximum",maximum)

console.log("axisIncrement",axisIncrement)

//                         range = maxValue-minValue;
// console.log("range",range)

//                         exponent = parseInt(Math.log10(range));

// console.log("exponent",exponent)


//                         magnitude = Math.pow(10, exponent);

//                         if(maxValue<5){
//                             magnitude = 10;
//                         }

// console.log("magnitude",magnitude)

//                         maxValue = Math.ceil10(maxValue/magnitude)*magnitude;

// console.log("topValue",maxValue)

                        // maxValue = maxValue + (maxValue * chartData.breathingRoom)

                        // var roundAmount = 1;
                        // if(maxValue>9){
                        //     roundAmount = 10;
                        // }else if(maxValue>99){
                        //     roundAmount = 100;
                        // }else if(maxValue>999){
                        //     roundAmount = 1000;
                        // }else if(maxValue>9999){
                        //     roundAmount = 10000;
                        // }else if(maxValue>99999){
                        //     roundAmount = 100000;
                        // }

//                         // maxValue = Math.ceil(maxValue / roundAmount) * roundAmount; //ceil
//                         axisIncrement = maxValue / 5;

// console.log("axisIncrement",axisIncrement)

// axisIncrement = Math.ceil(axisIncrement/magnitude)*magnitude;

// console.log("newaxisIncrement",axisIncrement)
//                         // axisIncrement  = Math.ceil(axisIncrement  / 10) * 10; //ceil

//                         //round up incrmement and make new max
//                         // axisIncrement = Math.ceil(axisIncrement/ 5) * 5;
                        // maxValue = axisIncrement * 5;

                        axisIncrementValue = 0;//Math.ceil(maxValue/1000)*1000

// axisIncrement = calcStepSize(maxValue,5)



                        for (var i = 0; i < 6; i++) {

                            axisIncrementValue = axisIncrementValue + axisIncrement;//Math.round(axisIncrementValue - axisIncrement);
                            labelTemplate = '<div class="dashchart-labels-set-item ' + (i == 0 ? 'is-zero' : '') + '">' + axisIncrementValue + '</div>'
                            $(".dashchart-labels-set.is-positive .dashchart-labels-set-list").prepend(labelTemplate);
                            $(".dashchart-labels-set.is-negative .dashchart-labels-set-list").append(labelTemplate);



                        };

                        maxValue = maximum ;//axisIncrementValue * 5;

                        $(".dashchart-values-list").width((chartData.values.length * 39) - 5)//for the last item
                        for (var i = 0; i < chartData.values.length; i++) {
                            var value = chartData.values[i];
                            valueCaption = getValueCaption(i, data.data)
                            valueLabel = value.label;
                            if ($("#metercharttype").val() == "daily" && !(i % 2 == 0)) {
                                valueLabel = "";
                            }
                            valueTemplate = '<div class="dashchart-values-item' +
                             (valueCaption ? ' is-' + valueCaption.type : '') + '">' +
                             (valueCaption ? '<div class="dashchart-values-item-caption">' + valueCaption.label + '</div>' : '') +
                             '<div class="dashchart-values-item-positive">' +
                             '<div class="dashchart-values-item-bar" style="height:' + (value.positive / (maxValue) * 100) + '%"></div>' +
                             '</div>' +
                             '<div class="dashchart-values-item-label"><div class="dashchart-values-item-label-inner">' + valueLabel + '</div></div>' +
                             (data.hasSolar ? '<div class="dashchart-values-item-negative">' + '<div class="dashchart-values-item-bar" style="height:' + (value.negative / (axisIncrement*5) * 100) + '%"></div>' + '</div>' : '') + '</div>';
                            $(".dashchart-values-list").append(valueTemplate);
                        };




                    // };


                $(".dashchart-values-item:eq("+selectValueIndex+")").trigger("click");



                    window.requestAnimationFrame(function() {

                    setTimeout(function() {
                            $(".dashmeter-chart").addClass("is-animations-enabled");
                            $(".dashmeter-chart").addClass("is-animations-animate");
                            showScroll();
                        },100)

                        })

                        // console.log(document.readyState)
                        // // The basic check
                        // if(document.readyState === 'complete') {
                        //     // good to go!
                        //     startAnimations();
                        // }else{
                        //     $(window).on("load",function(){
                        //             startAnimations();
                        //     });
                        // }


}


// var calcStepSize = function(range, targetSteps)
// {

// var ln10 = Math.log(10);

//   // calculate an initial guess at step size
//   var tempStep = range / targetSteps;

//   // get the magnitude of the step size
//   var mag = Math.floor(Math.log(tempStep) / ln10);
//   var magPow = Math.pow(10, mag);

//   // calculate most significant digit of the new step size
//   var magMsd = Math.round(tempStep / magPow + 0.5);

//   // promote the MSD to either 1, 2, or 5
//   if (magMsd > 5.0)
//     magMsd = 10.0;
//   else if (magMsd > 2.0)
//     magMsd = 5.0;
//   else if (magMsd > 1.0)
//     magMsd = 2.0;
//   else
//     magMsd = 1.0;

//   return magMsd * magPow;
// };




    function getValueCaption(period, data) {
        for (var i = 0; i < data.periodCaptions.length; i++) {
            if ((period + 1) == data.periodCaptions[i].periodStart) {
                return data.periodCaptions[i];
            }
        };
        return false;
    }

    // function switchChart() {
    //     var spendOrUsage = $("#dashmeter-spendvsusage").prop("checked") ? "spend" : "usage";
    // }

    function showScroll() {
        // window.requestAnimationFrame(function(){
        $(".dashmeter-chart").addClass("is-showing-scroll-animation");
        // });
        // setTimeout
    }
    return {
        init: init
    }
}(window.jQuery));







function getNiceNumber(x, round) {

        var exp = Math.floor(Math.log(x) / Math.LN10);
        var f = x / Math.pow(10, exp);
        var nf;

        if (round) {
            if (f < 1.5)
                nf = 1;
            else if (f < 3)
                nf = 2;
            else if (f < 7)
                nf = 5;
            else
                nf = 10;
        }
        else {
            if (f <= 1)
                nf = 1;
            else if (f <= 2)
                nf = 2;
            else if (f <= 5)
                nf = 5;
            else nf = 10;
        }

        return Number((nf * Math.pow(10, exp)).toFixed(20));
    }





// Closure
(function() {
  /**
   * Decimal adjustment of a number.
   *
   * @param {String}  type  The type of adjustment.
   * @param {Number}  value The number.
   * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
   * @returns {Number} The adjusted value.
   */
  function decimalAdjust(type, value, exp) {
    // If the exp is undefined or zero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  // Decimal round
  if (!Math.round10) {
    Math.round10 = function(value, exp) {
      return decimalAdjust('round', value, exp);
    };
  }
  // Decimal floor
  if (!Math.floor10) {
    Math.floor10 = function(value, exp) {
      return decimalAdjust('floor', value, exp);
    };
  }
  // Decimal ceil
  if (!Math.ceil10) {
    Math.ceil10 = function(value, exp) {
      return decimalAdjust('ceil', value, exp);
    };
  }
})();
