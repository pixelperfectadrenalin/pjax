App.dashplanpricing = (function($) {
    "use strict";
    var _Initialised = false;



    function init() {
        if (!_Initialised && $(".dashplanpricing").length) {
            _Initialised = true;




            initEvents();

            equalHeight();

            $(window).on("debouncedresize", function() {
              App.dashplanpricing.equalHeight();
            })


            $(window).on("load", function() {
              App.dashplanpricing.equalHeight();
            })



        }
    }

    function initEvents() {

      $(".dashplanpricing-details-concessions-item-addanother").on("click",function(){


            App.popupdash.openPopup('.popup-editform--concession', function() {
                // initLiveUsage();
              App.form.resetForm($(".popup-editform--concession"));
              $(".popup-editform-concessiontype").hide();

            }, function() {
                // removeLiveUsage();
            },this)


      });



        $("[name=concessiontype]").on("change",function(){

          var animationDuration = $.magnificPopup.instance.isOpen?400:0;

          $(".popup-editform-concessiontype").not(".is-"+$(this).val()).slideUp(animationDuration);

          $(".popup-editform-concessiontype.is-"+$(this).val()).slideDown(animationDuration);

        });








        $(".dashplanpricing-details-concessions-item-addanother").on("click",function(){

            var paymentType;

            if($(this).hasClass("is-creditcard")){
                paymentType = "creditcard"
            }else if($(this).hasClass("is-bank")){
                paymentType = "bank"
            }

            App.form.resetForm($(".popup-editform--concession"));


            openPopup("add",this)

        })


        $(".dashplanpricing-details-concessions-item-tools-item.is-delete").on("click",function(){

            var $theItem  = $(this).parents(".dashplanpricing-details-concessions-item");

            var theID = $theItem.find("input[name=id]").val();

            if(confirm("Do you really want to delete the concession?")){
                $("#concessionid").val(theID)
                $(".dashplanpricing-form--delete")[0].submit();
            }




        })



        $(".dashplanpricing-details-concessions-item-tools-item.is-edit").on("click",function(){

          var $item = $(this).parents(".dashplanpricing-details-concessions-item");


            App.form.resetForm($(".popup-editform--concession"));

            App.form.copyValuesFromFormToForm($item, $(".popup-editform--concession"))



            openPopup("edit",$item[0]);


        })

    }








function openPopup(addOrEdit,callerElement){



        var titleVerb;
        var buttonCaption;
        if(addOrEdit=="add"){
            titleVerb = "Add";
            buttonCaption = "Save Details"
        }else if(addOrEdit=="edit"){
            titleVerb = "Edit";
            buttonCaption = "Update Details"
        }


          $(".popup-editform--concession .button-popupsubmit").text(buttonCaption)


        $(".popup-editform-intro-title").text(titleVerb+" your concession");


        App.popupeditform.openPopup('.popup-editform--concession',callerElement)


        // $.magnificPopup.open({

        //     // prependTo: $("form:first"),
        //     // delegate: 'a',
        //     items: {
        //         src: '.popup-editform--concession',
        //         type: 'inline'
        //     },
        //     // tLoading: 'Loading image #%curr%...',
        //     mainClass: 'mfp-fade popup-editform-outer',


        //     closeBtnInside: true,
        //     preloader: false,
        //     removalDelay: 150, // Delay to allow for fade out
        // });
  }


  function equalHeight(){

    App.equalHeight.equalHeight({elements:".dashplanpricing-details-concessions-item"});

  }



    return {
        init: init,
        equalHeight: equalHeight
    }
}(window.jQuery));
