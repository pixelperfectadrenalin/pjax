App.sharetogglebutton = (function($) {
  "use strict";
  var _Initialised = false;

  function init() {
    if (!_Initialised && $(".detailheader-panel-share").length) {
      _Initialised = true;
      initEvents();
    }
  }

  function initEvents() {

      $(".detailheader-panel-share").on("click",function(){
          if($(this).hasClass("is-show")){
            return false;
          }
          else{
            $(this).addClass("is-show");

          }

      });
  }




  return {
    init: init
  }
}(window.jQuery));
