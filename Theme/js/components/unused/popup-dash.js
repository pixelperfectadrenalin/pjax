App.popupdash = (function($) {
    "use strict";
    var _Initialised = false;



    function init() {
        if (!_Initialised && $(".popup-dash").length) {
            _Initialised = true;
            initEvents();

        }
    }




    function initEvents() {



    }





    function openPopup(element, onOpen, onClose, callerElement){


      var animation;

      if(callerElement){
          animation = "mfp-zoomin"

      }else{

        animation = "mfp-fade"

      }


                $.magnificPopup.open({

                    // prependTo: $("form:first"),
                    // delegate: 'a',
                    items: {
                        src: element,
                        type: 'inline'
                    },
                    // tLoading: 'Loading image #%curr%...',
                    mainClass: 'popup-dash-outer '+animation,


                    closeBtnInside: true,
                    preloader: false,
                    removalDelay: 550, // Delay to allow for fade out


                    callbacks: {


                      buildControls: function() {
                        // re-appends controls inside the main container
                        // this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                      },

                      open: function() {
                        // Will fire when this exact popup is opened
                        // this - is Magnific Popup object

                        App.popupeditform.onMagnificOpen(callerElement);

                        if(typeof onOpen == "function"){
                          onOpen()
                        }

                      $(".js-popup-dash-close").on("click.popupdash",function(){

                        $.magnificPopup.close();

                      })


                      },
                      close: function() {
                        // Will fire when popup is closed
                        if(typeof onClose == "function"){
                          onClose()
                        }

                        $(".js-popup-dash-close").off("click.popupdash");

                      },
                      beforeClose:function(){

                        App.popupeditform.onMagnificClose(callerElement);

                      },

                      // close: function() {
                      //   // console.log('Popup removal initiated (after removalDelay timer finished)');
                      // },
                      // afterClose: function() {
                      //   // console.log('Popup is completely closed');
                      // }
                    }

                });



    }



    return {
        init: init,
        openPopup: openPopup
    }
}(window.jQuery));
