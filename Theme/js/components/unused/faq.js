App.faqaccordion = (function($) {
	"use strict";

	function init() {
		if ($(".faq").length) {
			initCategories();
		}
	}

	function initCategories() {
		$('.faq-section-reed').openClose({
			activeClass: 'is-accordion-reed-open',
			opener: '.faq-section-reed-title',
			slider: '.faq-section-reed-content',
			animSpeed: 400,
			effect: 'slide'
		});


    $(".js-faq-button-seeall").on("click",function(){

      var $element = $(this);
      var $theFAQBlock = $element.closest(".faq-block");


        $theFAQBlock.find(".faq-section-more").stop().slideDown().addClass("is-active");

        $element.stop().slideUp();


    });

	}


	return {
		init: init
	}
}(window.jQuery));
