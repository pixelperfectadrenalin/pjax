/*global App  */
App.smoothScrollingLinks = (function($) {
  "use strict";
  var _Initialised = false;
  var oldDirection;
  var direction;
  var previous_scroll;

  function init() {
    if (!_Initialised) {
      _Initialised = true;
      initEvents();
    }
  }

  function initEvents() {
    $('body').on('click', 'a[href^="#"]', function(e) {
      var target = $(this).attr('href');
      var $target = $(target);
      //If blank hash, prevent default jump then do nothing
      if (target === "#" || $(this).hasClass("js-lightbox-link")) {
        e.preventDefault();
        //Else if target exists prevent default jump and smooth scroll to target
      } else if ($target.length) {
        e.preventDefault();
        scrollToElement($target);
      }
    });
  }

  function scrollToElement($target, callback) {
    $target = $($target);

    var scroll = $(window).scrollTop();
    previous_scroll = scroll;

    if ($target.length > 0) {
      var scroll_change = $target.offset().top - previous_scroll;
      if (scroll_change < 0) {
        direction = "down";
      } else {
        direction = "up";
      }
      var elementTop = $target.offset().top - getOffsets(direction);


      if (elementTop === $(window).scrollTop()) {
        if (typeof callback === "function") {
          callback();
        }
      } else {
        $('html, body').stop().animate({
          'scrollTop': elementTop

        }, 1000, $.bez([0.77, 0, 0.175, 1]), function() {
          if (typeof callback === "function") {
            callback();
          }
        });
      }
    } else {
      if (typeof callback === "function") {
        callback();
      }
    }
  }

  function getOffsets(direction) {
    // var navigationPrimary = 0;
    // var navigationSub = 0;
    // if ($(window).width() >= 1000) {
    //  navigationPrimary = $(".container-navigation-primary-sticky-wrapper").height();
    //  if (direction == "up") {
    //    navigationSub = 0;
    //  } else {
    //    navigationSub = $(".container-navigation-sub").height();
    //  }
    // }
    // return navigationPrimary + navigationSub;
    return 0;
  }
  return {
    init: init,
    scrollToElement: scrollToElement
  };
}(window.jQuery));
