App.balanceText = (function($) {
  "use strict";
  var _Initialised = false;
  var $elements;

  function init() {
    if (!_Initialised) {
      _Initialised = true;
      $(window).on("debouncedresize", function() {
        App.balanceText.update();
      })
    }
  }

  function update() {
    if ($elements.length) {
      $elements.each(function() {
        $(this).balanceText($(this).data("balancetext"))
      })
    }
  }

  function add($element, options) {
    $element.data("balancetext", options);
    if ($elements) {
      $elements = $elements.add($element);
    } else {
      $elements = $element;
    }
    $element.balanceText(options);
  }
  return {
    init: init,
    add: add,
    update: update
  }
}(window.jQuery));
