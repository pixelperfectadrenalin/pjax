App.detailtabs = (function($) {
  "use strict";
  var _Initialised = false;
  var _hasPlayed = false;

  function init() {
    if (!_Initialised && $(".detailtabs").length) {
      _Initialised = true;
      initEvents();
    }
  }

  function initEvents() {


      $(".detailtabs-mobilemenu select").on("change",function(){

        window.location = $(this).val();

      });
      
      // mobileTabs();
      // deskTabs();



  }



    function mobileTabs(){
       $(".detailtabs-mobilemenu select").on("change",function(){

            $(".detailtabs-item").removeClass("is-active")
            $(this).addClass("is-active");

            var tabs = $(this).val();

            var tabsid = tabs.split("-").pop(-1);

            $(".tabcontent").addClass("is-invisible");    
            $(".tabcontent-"+tabsid).removeClass("is-invisible");

      });
    }



    function deskTabs(){
    $(".detailtabs-item").click(function(event) {

            event.preventDefault();

            $(".detailtabs-item").removeClass("is-active")
            $(this).addClass("is-active");

            var tabs = $(this).data("tab");

            var tabsid = tabs.split("-").pop(-1);

            $(".tabcontent").addClass("is-invisible");    
            $(".tabcontent-"+tabsid).removeClass("is-invisible");
              
        });
  }



  return {
    init: init
  }
}(window.jQuery));
