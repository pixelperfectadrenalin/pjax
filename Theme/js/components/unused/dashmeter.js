App.dashmeter = (function($) {
    "use strict";
    var _Initialised = false;
    var currentPeriod;
    var previousPeriod;
    var previousPeriodDisabled;
    var nextPeriod;
    var nextPeriodDisabled;
    var chartData;
    var maxValue;
    var minValue;
    var topValue;
    var range;
    var exponent;
    var exponentLen;
    var magnitude;
    var value = 0;
    var valueCaption;
    var divide5;
    var increment;
    var incrementValue;
    var $labelSet;
    var labelTemplate;
    var valueTemplate;
    var valueLabel;
    var $scrollAnimationDiv;
    var chartTypes = ["spend", "usage"];
    var isSmoothInit = false;
    var newScrollState;
    var oldScrollState;
    var selectValueIndex = 0;
    var spendOrUsage = "spend";
    var ajaxData;

    function init() {
        if (!_Initialised && $(".dashmeter").length) {
            _Initialised = true;
            $scrollAnimationDiv = $("<div>");
            FastClick.attach($(".dashmeter-chart-chart")[0]);
            initEvents();
        }
    }

    function initEvents() {
        initChart();
        if (!Modernizr.touchevents) {
            $('.dashchart-content:first').smoothWheel();
            isSmoothInit = true;
            $('.dashchart-content').kinetic({
                moved: function() {
                    if (isSmoothInit) {
                        $('.dashchart-content:first').smoothWheel({
                            remove: true
                        });
                        isSmoothInit = false;
                    }
                },
                stopped: function() {
                    if (!isSmoothInit) {
                        $('.dashchart-content:first').smoothWheel();
                        isSmoothInit = true;
                    }
                }
            });
        }
    }

    function initChart() {
        currentPeriod = getQueryVariable("ID");
        currentPeriod = (currentPeriod === false || currentPeriod === "") ? "now" : currentPeriod;
        loadChartData(currentPeriod);
        $(".dashmeter-intro-lastupdated-icon").on("click", function() {
            loadChartData(currentPeriod);
        });
        $("#dashmeter-spendvsusage").on("change", function() {
            spendOrUsage = $("#dashmeter-spendvsusage").prop("checked") ? "spend" : "usage";
            renderChart();
        });
        $(".dashmeter-datetoggle-date-prev").on("click", function() {
          if(!previousPeriodDisabled){
              loadChartData(previousPeriod);
            }
        });
        $(".dashmeter-datetoggle-date-next").on("click", function() {
          if(!nextPeriodDisabled){
            loadChartData(nextPeriod);
          }
        });
        $(document).on("click", ".dashchart-values-item", function() {
            $(".dashchart-values-item").not(this).removeClass("is-active");
            $(this).addClass("is-active");
            chartData = ajaxData.data[spendOrUsage];
            selectValueIndex = $(this).index();
            var theValue = ajaxData.data[spendOrUsage].values[selectValueIndex];
            $(".is-smartmeter .dashmeter-chart-info-data-item-value").text(chartData.valuePattern.replace("[X]", theValue.positive.toFixed(2)));
            $(".is-solarsavings .dashmeter-chart-info-data-item-value").text(chartData.valuePattern.replace("[X]", theValue.negative.toFixed(2)));
            $(".dashmeter-chart-info-date-value").text(theValue.longlabel);
            $(".dashmeter-chart-info-link a").toggle($("#metercharttype").val() == "monthly").attr("href", $(".dashmeter-chart-info-link a").attr("href-src").replace("[X]", theValue.ID));
        });
    }

    function loadChartData(period) {
        $.ajax({
            dataType: "json",
            url: $("#metercharturlstart").val() + $("#metercharttype").val() + $("#metercharturlmiddle").val() + period + $("#metercharturlend").val(),
            error: function(data) {
                console.log("Error", data);
            },
            success: function(data) {
                ajaxData = data;
                renderChart();
            }
        });
    }

    function renderChart() {
        var data = ajaxData;
        chartData = data.data[spendOrUsage];
        previousPeriod = data.previousPeriod.id;
        previousPeriodDisabled = (data.previousPeriod.disabled === true);
        nextPeriod = data.nextPeriod.id;
        nextPeriodDisabled = (data.nextPeriod.disabled === true);
        currentPeriod = data.currentPeriod.id;
        maxValue = 0;
        minValue = 9999999999999;
        $(".dashmeter-intro-lastupdated-copy").text("Last updated on " + data.updateddatetime);
        $(".dashmeter-chart").toggleClass("has-negative", data.hasSolar);
        if ($("#metercharttype").val() == "daily") {
            $(".dashspend-ctas-item-button.is-viewmonthly").attr("href", $(".dashspend-ctas-item-button.is-viewmonthly").attr("href-src").replace("[X]", data.currentMonth.id));
        }
        $(".dashmeter-chart-info-data-item.is-solarsavings").toggle(data.hasSolar);
        $(".dashchart-values-item-negative").toggle(data.hasSolar);
        $(".dashchart-labels-set.is-negative").toggle(data.hasSolar);
        $(".dashmeter-datetoggle-date-current").text(data.currentPeriod.label);
        $(".dashmeter-datetoggle-date-prev").attr("title", data.previousPeriod.label).toggleClass("is-disabled",previousPeriodDisabled);
        $(".dashmeter-datetoggle-date-next").attr("title", data.nextPeriod.label).toggleClass("is-disabled",nextPeriodDisabled);
        $(".dashchart-values-list").empty();
        $(".dashchart-labels-set-list").empty();
        $(".dashmeter-chart").removeClass("is-animations-enabled");
        $(".dashmeter-chart").removeClass("is-animations-animate");
        $(".is-positive .dashchart-labels-set-caption").text(chartData.positiveLabel);
        $(".is-negative .dashchart-labels-set-caption").text(chartData.negativeLabel);
        $(".dashchart-labels-unit").text(chartData.unit);
        $(".dashchart-labels-unit").text(chartData.unit).toggleClass("is-using-systemfont", chartData.unit === "$" || chartData.unit === "¢");
        $(".is-smartmeter .dashmeter-chart-info-data-item-label").text(chartData.positiveToolLabel);
        $(".is-solarsavings .dashmeter-chart-info-data-item-label").text(chartData.negativeToolLabel);
        chartData.values.forEach(function(value, index, array) {
            maxValue = Math.max(maxValue, value.positive);
            minValue = Math.min(minValue, value.positive);
            if (data.hasSolar) {
                maxValue = Math.max(maxValue, value.negative);
                minValue = Math.min(minValue, value.negative);
            }
        });
        exponentLen = parseInt(maxValue).toString().length;
        exponent = exponentLen - 2;
        magnitude = Math.pow(10, exponent);
        if (maxValue > 5) {
            magnitude = Math.ceil(magnitude);
        }
        divide5 = maxValue / 5;
        increment = Math.ceil(divide5 / magnitude) * magnitude;
        maxValue = (increment * 5);
        for (var i = 0; i <= 5; i++) {
            if (maxValue < 5) {
                incrementValue = (increment * i).toFixed(1);
            } else {
                incrementValue = (increment * i);
            }
            labelTemplate = '<div class="dashchart-labels-set-item ' + (i === 0 ? 'is-zero' : '') + '">' + incrementValue + '</div>';
            $(".dashchart-labels-set.is-positive .dashchart-labels-set-list").prepend(labelTemplate);
            $(".dashchart-labels-set.is-negative .dashchart-labels-set-list").append(labelTemplate);
        }
        $(".dashchart-values-list").width((chartData.values.length * 39) - 5); //for the last item
        chartData.values.forEach(function(value, index, array) {
            valueCaption = getValueCaption(index, data.data);
            valueLabel = value.label;
            if ($("#metercharttype").val() === "daily" && !(index % 2 === 0)) {
                valueLabel = "";
            }
            valueTemplate = '<div class="dashchart-values-item' + (valueCaption ? ' has-period-caption is-' + valueCaption.type : '') + '">' + (valueCaption ? '<div class="dashchart-values-item-caption">' + valueCaption.label + '</div>' : '') + '<div class="dashchart-values-item-positive">' + '<div class="dashchart-values-item-bar" style="height:' + (value.positive / maxValue * 100) + '%"></div>' + '</div>' + '<div class="dashchart-values-item-label"><div class="dashchart-values-item-label-inner">' + valueLabel + '</div></div>' + (data.hasSolar ? '<div class="dashchart-values-item-negative">' + '<div class="dashchart-values-item-bar" style="height:' + (value.negative / maxValue * 100) + '%"></div>' + '</div>' : '') + '</div>';
            $(".dashchart-values-list").append(valueTemplate);
        });
        $(".dashchart-values-item:eq(" + selectValueIndex + ")").trigger("click");
        window.requestAnimationFrame(function() {
            setTimeout(function() {
                $(".dashmeter-chart").addClass("is-animations-enabled");
                $(".dashmeter-chart").addClass("is-animations-animate");
                showScroll();
            }, 100);
        });
    }

    function getValueCaption(period, data) {
        for (var i = 0; i < data.periodCaptions.length; i++) {
            if ((period + 1) === data.periodCaptions[i].periodStart) {
                return data.periodCaptions[i];
            }
        }
        return false;
    }

    function showScroll() {
        $(".dashmeter-chart").addClass("is-showing-scroll-animation");
    }

    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] === variable) {
                return pair[1];
            }
        }
        return (false);
    }
    return {
        init: init
    };
}(window.jQuery));
