App.chart = (function($) {
  "use strict";
  var _Initialised = false;
  var _hasPlayed = false;

  var labelCount = 10;

  function init() {
    if (!_Initialised && $(".chart").length) {
      _Initialised = true;
      initEvents();
    }
  }

  function initEvents() {


    //Find max and min

    var min = chartdata.values[0].value;

    var max = chartdata.values[0].value;



    for (var i = 0; i < chartdata.values.length; i++) {

      var value = chartdata.values[i].value;


      if(value<min){
        min = value;
      }

      if(value>max){
        max = value;
      }


    };

    if (min<0){
      min = 0;
      max = max-min;
    }

    var min = Math.round(min / 10) * 10;// round down to nearest 10

    var max = Math.ceil(max / 10) * 10;// round up to nearest 10


    var difference = (max-min);


    var increment = difference/labelCount;

    var increment = Math.round(increment / 10) * 10;// round down to nearest 10

    var theMaxAxis = max;//difference/(labelCount+4)*increment;



    // min = min - increment;

    // max = max + increment;




    for (var i = 0; i < chartdata.values.length; i++) {

      var value = chartdata.values[i].value;
      var label = chartdata.values[i].label;

      $(".chart-value-list").append('<div class="chart-value-item"><div class="chart-value-item-bar" style="height:0;"></div></div>')


      $(".chart-content-valuelabels-list").append('<div class="chart-content-valuelabels-item"><div class="chart-content-valuelabels-item-label"><div class="chart-content-valuelabels-item-label-inner">' + label + '</div></div></div>')

    }





// console.log("min", min)
// console.log("max", max)

// console.log("difference", difference)

// console.log("increment", increment)



    var start = min ;
    var end = max;
    // var increment = increment*-1;


    console.log("start",start);

    console.log("end",end);

    console.log("difference",difference);

    console.log("increment",increment);


    for (var i = start; i < end; i=i+increment) {


      $(".chart-label-list").prepend('<div class="chart-label-item"><div class="chart-label-item-inner"><div class="chart-label-item-inner2">'+parseInt(i)+'</div></div></div>')


      $(".chart-grid-list").prepend('<div class="chart-grid-item"><div class="chart-grid-item-inner"></div></div>')

    };


      // $(".chart-label-list").append('<div class="chart-label-item"><div class="chart-label-item-inner"><div class="chart-label-item-inner2">'+(0)+'</div></div></div>')

      // $(".chart-grid-list").append('<div class="chart-grid-item"><div class="chart-grid-item-inner"></div></div>')



    for (var i = 0; i < chartdata.values.length; i++) {

      var value = chartdata.values[i].value;
      var label = chartdata.values[i].label;

      if(value>0){
          $(".chart-content-positive .chart-value-item").eq(i).find(".chart-value-item-bar").css({"height":((value/theMaxAxis)*100)+'%'},2000);
      }else{
          $(".chart-content-negative .chart-value-item").eq(i).find(".chart-value-item-bar").animate({"height":((value/theMaxAxis)*100)+'%'},2000);
      }

    }



  }

  return {
    init: init
  }
}(window.jQuery));
