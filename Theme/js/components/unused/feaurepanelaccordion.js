App.featurepaenlaccordion = (function($) {
	"use strict";

	function init() {
		if ($(".featurepanel-list-inner").length) {
		//	initCategories();

				initEvents();
			
		}
	}

	// function initCategories() {
	// 	$('.accordion-category').openClose({
	// 		activeClass: 'is-accordion-category-open',
	// 		opener: '.accordion-category-title',
	// 		slider: '.accordion-category-content',
	// 		animSpeed: 400,
	// 		effect: 'slide'
	// 	});
	// }

	 function initEvents() {


            //initReeds();
            

            initSlide();
            initReeds();

            $(window).on("debouncedresize", function() {
            	initSlide();
            	
            })


  	}

	function initSlide(){
	  	var $openerparent = $(".featurepaenlaccordion-reed");

	  	if(viewport().width<1024){
	  		$openerparent.removeClass("is-blocks");
	  		

	  	}
	  	else		  		
	  		$openerparent.addClass("is-blocks");
	}




	function initReeds(){

		var $opener = $(".featurepaenlaccordion-reed-title");

		$opener.on("click", function(){

				var $openerparent = $(this).closest(".featurepaenlaccordion-reed");

				if( !$openerparent.hasClass("is-blocks")){

					if( !$openerparent.hasClass("is-open")){
      					$(".featurepaenlaccordion-reed-content-inner2").css("height","auto");
						$(this).closest(".featurepaenlaccordion-reed").find(".featurepaenlaccordion-reed-content").slideDown();
						$openerparent.addClass("is-open");
					}
					else{
						$(this).closest(".featurepaenlaccordion-reed").find(".featurepaenlaccordion-reed-content").slideUp();
						$openerparent.removeClass("is-open");

					}

				}
	
		})

	}
	return {
		init: init
	}
}(window.jQuery));
