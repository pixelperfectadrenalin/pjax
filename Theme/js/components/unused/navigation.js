/*global App  */
App.navigation = (function($) {
  "use strict";
  var _Initialised = false;

  var searchTimer;

  var allowSearch;

  function init() {
    if (!_Initialised) {
      _Initialised = true;
      initEvents();
      handleResize();
    }
  }

  function initEvents() {


      initSearch();
      skipToContentClick();



    $(".headermobile-menutoggle").on("click", function(e) {
      e.preventDefault();
      if ($("#page").hasClass("is-navigation-open")) {
        closeNavigation();
      } else {
        closeSearch();
        openNavigation();
      }
    });





    $(".headermobile-searchtoggle").on("click", function(e) {
      if ($("#page").hasClass("is-search-open")) {
      e.preventDefault();
        closeSearch();
      } else {
        closeNavigation();
        openSearch();

      }
    });



    // $(".navigation-secondary-item-label--desktop").on("click", function(e) {
    //   if ($("#page").hasClass("is-search-open")) {
    //     e.preventDefault();
    //     $("#page").removeClass("is-search-open");
    //   } else {
    //     $("#page").addClass("is-search-open");
    //   }
    // });
    $(".navigation-bodyblocker").on("click", function(e) {
      e.preventDefault();
      closeNavigation();
      closeSearch();
      // closeSubNavigation();
    });

    // $(".navigation-primary-item--has-children .navigation-primary-item-label").on("click", function(e) {
    //   if (Modernizr.touch || viewport().width < 980) {
    //     var theParentItem = $(this).parents(".navigation-primary-item");
    //     // theParentItem.toggleClass("is-menu-active");
    //     if (!theParentItem.hasClass("is-menu-active")) {
    //       // e.preventDefault();
    //       e.preventDefault();
    //       openNavigation();
    //       openSubNavigation()
    //       $(".navigation-primary-item--has-children").not(theParentItem).removeClass("is-menu-active").find(".navigation-sub").stop(true).slideUp(400);
    //       theParentItem.addClass("is-menu-active");
    //       theParentItem.find(".navigation-sub").stop(true).hide().slideDown(400,handleResize);

    //     } else {
    //       //do nothing allow link to just work
    //     }
    //   }
    // });
    $(window).on("resize", function() {
      App.navigation.handleResize();
    })
  }


   function skipToContentClick() {
        $('.skip').click(function (e) {
            e.preventDefault();
            
            var next = $('.breadcrumbs a:first,.herohome a:first');
            if (next.length > 0) {
                $(next[0]).focus();
            }

            $('html, body').animate({
                scrollTop: $(".herohome, .breadcrumbs").offset().top
            }, 500);
        });
    }



  function calculateNavigationHeight() {
    var headerHeight = 54;
    var navigationHeight = $(".navigation-full").height() + headerHeight;
    var windowHeight = $(window).height();
    $("#page").height("auto");
    var bodyHeight = $("#page").height();
    var smallestHeight = ((windowHeight < navigationHeight) ?
      navigationHeight : windowHeight);
    if (smallestHeight < bodyHeight) {
      $("#page").height(smallestHeight);
      $("#page").addClass("is-navigation-open-overflow-hidden");
    } else {
      $("#page").height("auto");
      $("#page").removeClass("is-navigation-open-overflow-hidden");
    }
  }




  function openNavigation() {
    $("#page").addClass("is-navigation-open");
    calculateNavigationHeight()
  }

  function closeNavigation() {
    $("#page").removeClass("is-navigation-open");
    $("#page").height("");
    // $(".navigation-primary-item--has-children").removeClass("is-menu-active");
  }

  function openSearch() {
    $("#page").addClass("is-search-open");
    // calculateNavigationHeight()
  }

  function closeSearch() {
    $("#page").removeClass("is-search-open");
    $("#page").height("");
    // $(".navigation-primary-item--has-children").removeClass("is-menu-active");
  }




  function initSearch(){


      $(".navigation-utility-search-field-input, .navigation-utility-search-field-button").focus(function () {
        $(".navigation-utility-search").addClass("is-focused");
      });



      // $(".navigation-utility-item navigation-utility-search").on("submit",function(e){


      //   if($(".navigation-utility-search-field-input:focus").length){

      //     e.preventDefault()

      //   }

      // })

      $(".navigation-utility-search-field-input, .navigation-utility-search-field-button").blur(function () {
          //Need to wait at least 300? ms before hiding the field + submit button or submit button click wouldn't ever register

          setTimeout(function () {
            if(!$(".navigation-utility-search-field-input:focus, .navigation-utility-search-field-button:focus").length){
            $(".navigation-utility-search").removeClass("is-focused");
            }
          }, 400);
      });


    //catch window blur, and blur fields, so clicking seach button to refocus window doesn't instantly submit form
    $(window).on("blur", function () {

      $(".navigation-utility-search-field-input, .navigation-utility-search-field-button").blur();

    });



    // $(".navigation-utility-search form").on("submit",function(e){

    //   console.log()

    //     if(allowSearch == false){

    //       e.preventDefault()

    //     }

    // });




  }


  // function openSubNavigation(){

  //   $("#page").addClass("is-subnavigation-open");

  // }


  // function closeSubNavigation(){

  //   $("#page").removeClass("is-subnavigation-open");

  // }
    // function openSearch() {
    //   closeNavigation();
    //   $("#site").addClass("is-search-open");
    // }



    // function closeSearch() {
    //   $("#site").removeClass("is-search-open");
    // }



  function handleResize() {

    if (viewport().width < 980 && $("#page").hasClass("is-navigation-open")) {
      //mobile
      calculateNavigationHeight()
    }
  }
  return {
    init: init,
    handleResize: handleResize
  };
})(jQuery);
