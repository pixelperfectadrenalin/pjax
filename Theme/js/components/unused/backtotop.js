App.backtotop=(function () {

	//Global Variables
	var _Initialised = false;
	function init(){

		//Prevent double initialisation
		// if(!_Initialised && $( '[rel~=tooltip]' ).length){
		if($('.backtotop').length){
			_Initialised = true;
			initEvents();
		}
	}


	function update(){

		initEvents();



	}

	//Initialize module
	function initEvents(){
		var $button = $('.backtotop'),
	        timeoutDelay = 200,
	        $window,
	        scrollTopTimeout;

	    $button.on('click', function() {
	        $('html, body').animate({scrollTop: 0}, 'fast');
	        return false;
	    });

	    $(window).on('scroll', function() {
	        clearTimeout(scrollTopTimeout);
	        scrollTopTimeout = setTimeout(function () {
	            $window = $(this);
	            if ($window.height() < $window.scrollTop()) {
	                $button.stop().fadeIn('fast');
	            } else {
	                $button.stop().fadeOut('fast');
	            }
	        }, timeoutDelay);
	    });	
   
	}



	//Return public variables
	return {
		init:init
	}
})();


