App.formCreditcard = (function($) {
  "use strict";
  var _Initialised = false;
  var didValidate = false;


  function init() {
    if (!_Initialised && $(".form-creditcard").length) {
      _Initialised = true;
      initForm();
    }
  }

  function initForm() {
    initCCNumberField();
  }



  function initCCNumberField() {
    $('.form-creditcard-ccnumber').bind('input', function() {
      var c = this.selectionStart,
          r = /[^0-9]/gi,
          v = $(this).val();
      if(r.test(v)) {
        $(this).val(v.replace(r, ''));
        c--;
      }
      this.setSelectionRange(c, c);
    });
  }


  function getCreditCardType(){
    var ccresult = $(".form-creditcard-ccnumber").validateCreditCard();

    if (ccresult.card_type !== null) {
      return ccresult.card_type.name;
    }
  }

  // function validateForm() {
  //   didValidate = true;

  //   var formIsValid = App.formValidate.getValidator().valid();

  //   return formIsValid;
  // }

  function highlightCard(result) {
    //console.log("highlightCard");
    if (result.card_type !== null) {
      //console.log(result.card_type.name);
      if (result.length_valid) {
          $(".form-creditcard-icons").children().removeClass("is-active");

          switch (result.card_type.name) {
              case "mastercard":
                  $(".form-creditcard-icons-mastercard").addClass("is-active");
                  //console.log(result.card_type.name);
                  break;
              case "visa":
                  $(".form-creditcard-icons-visa").addClass("is-active");
                  //console.log(result.card_type.name);
                  break;
              case "amex":
                  $(".form-creditcard-icons-amex").addClass("is-active");
                  //console.log(result.card_type.name);
                  break;
              // case "diners_club_international":
              //     $(".form-creditcard-icons-diners").addClass("is-active");
              //     //console.log(result.card_type.name);
              //     break;
              default:
                  $(".js-icon-cc").removeClass("is-active");
          }
      }
      else {
          $(".form-creditcard-icons").children().removeClass("is-active");
      }
    }
  }

  return {
    init: init,
    // validateForm: validateForm,
    // initForm: initForm,
    // getAmount: getAmount,
    // getAppeal: getAppeal,
    // getOption: getOption,
    getCreditCardType: getCreditCardType,
    highlightCard: highlightCard
  }
}(window.jQuery));
