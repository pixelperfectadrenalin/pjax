App.dashalerts = (function($) {
    "use strict";
    var _Initialised = false;

    var $theEditForm;
    var $theDeleteForm;

    function init() {
        if (!_Initialised && $(".dashalerts").length) {
            _Initialised = true;

            initEvents();
            $theEditForm  = $(".dashalerts-form--edit");
            $theDeleteForm  = $(".dashalerts-form--delete");

        }
    }

    function initEvents(){

        $(".dashalerts-alerts-item-tools-item.is-edit").on("click",function(){

            var $theItem  = $(this).parents(".dashalerts-alerts-item");


            App.form.copyValuesFromFormToForm($theItem, $(".dashalerts-form--edit"))

            $(".dashalerts-form--add").slideUp();
            $(".dashalerts-alerts").slideUp();
            $(".dashalerts-form--edit").slideDown();


        })


        $(".dashalerts-form-cancel-link").on("click",function(){

            $(".dashalerts-form--edit").slideUp();
            $(".dashalerts-form--add").slideDown();
            $(".dashalerts-alerts").slideDown();

        })

        $(".dashalerts-alerts-item-tools-item.is-delete").on("click",function(){

            var $theItem  = $(this).parents(".dashalerts-alerts-item");

            var theID = $theItem.find("[name=id]").val();

            if(confirm("Do you really want to delete the alert?")){
                $(".dashalerts-form--delete [name=id]").val(theID)
                $(".dashalerts-form--delete form")[0].submit();
            }

        })


        $(".dashalerts-form--add form").on("submit",function(){
          if($(this).valid()){
            $(this)[0].submit()
            }
        })


        $(".dashalerts-form--edit form").on("submit",function(){
          if($(this).valid()){
            $(this)[0].submit()
            }
        })



    }


    return {
        init: init
    }
}(window.jQuery));
