App.refineyouresearch = (function($) {
	"use strict";

	function init() {
		if ($(".js-refineyoursearch-toggle").length) {
			initRefineyouresearch();
		}
	}

	function initRefineyouresearch() {
		 $(".js-refineyoursearch-toggle-button").click(function() {
		        var theParent = $(this).parents(".js-refineyoursearch-toggle");
		        if ($(theParent).hasClass("is-open")) {
		          $(theParent).removeClass("is-open");
		          $(theParent).find(".js-refineyoursearch-toggle-content").slideUp(400);
		          $(this).text("Show more")
		        }
		        else {
		          $(theParent).addClass("is-open");
		          $(theParent).find(".js-refineyoursearch-toggle-content").slideDown(400);
		          $(this).text("Show less")
		        }
		      });
	}

	
	return {
		init: init
	}
}(window.jQuery));
