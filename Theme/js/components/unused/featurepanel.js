App.featurepanel = (function($) {
  "use strict";
  var _Initialised = false;
  var _hasPlayed = false;

  function init() {
    if (!_Initialised && $(".featurepanel-list-inner").length) {
      _Initialised = true;
      initEvents();
    }
  }

  function initEvents() {


            equalHeight();

            $(window).on("debouncedresize", function() {
              App.featurepanel.equalHeight();
            })

            $(window).on("load", function() {
              App.featurepanel.equalHeight();
            })




  }


  function equalHeight(){


      if(viewport().width>1024){
        App.equalHeight.equalHeight({elements:".featurepaenlaccordion-reed-content-inner2"});
      }
      else{
        $(".featurepaenlaccordion-reed-content-inner2").css("height", "auto")
      }
    
  }


  return {
    init: init,
    equalHeight: equalHeight
  }
}(window.jQuery));
