App.dashlayout = (function($) {
    "use strict";
    var _Initialised = false;
    var didValidate = false;

    var oldSize;

    var $animationDiv;

    var liveUsageInterval;
    var liveUsageIntervalIsFree = true;


    function init() {
        if (!_Initialised && $(".dashlayout").length) {
            _Initialised = true;
            initForm();

            $animationDiv = $("<div>");

        }
    }

    function initForm() {

        if ($(".dashlayout-sidebar-contact").length) {

            $("html").addClass("has-consultant")

        }


        initDashaccountcurrentbillingSwitch();

        initMobileNav();

        initSidebarMenu();

        stickyLogoSticky();

        initSidebarSicky()

        $(window).on("debouncedresize", function() {
            stickyLogoSticky();
        })


        if($("#liveusagebutton").length){


          initUsageEvents();

        }

    }


    function initUsageEvents() {
        $("#liveusagebutton").on("click", function() {
            // if($(this).parents("is-disabled")){return false}
            App.popupdash.openPopup('.popup-dash--liveusage', function() {
                initLiveUsage();
            }, function() {
                removeLiveUsage();
            },this)
        });
    }



    function initLiveUsage() {

        liveUsageInterval = setInterval(function() {
                loadLiveUsageData();
            }, 1 * 1) // 1 second
    }

    function removeLiveUsage() {
        liveUsageIntervalIsFree = true;
        clearInterval(liveUsageInterval);
    }

    function loadLiveUsageData() {
        if (!liveUsageIntervalIsFree) {
          //Polling too quickly - return back, and don't begin a new request
            return false;
        }

        //Otherwise, continue with a new request

        liveUsageIntervalIsFree = false;
        $.ajax({
            dataType: "json",
            url: $("#liveusageurl").val(),
            error: function(data) {
                console.log("Error", data)
                liveUsageIntervalIsFree = true;
            },
            success: function(data) {
                // data.usage = (Math.random()*2).toFixed(2);
                $(".popup-dash-liveusage-value").text("$" + data.usage);
                liveUsageIntervalIsFree = true;
            }
        });
    }




    function initSidebarMenu() {

        $("#dashlayout-sidebar-navigation").on("change", function() {

            window.location = $("#dashlayout-sidebar-navigation option:selected").val();


        })


        // $(".dashlayout-sidebar-menu-item-label").on("click", function() {


        //     var selectedIndex = $(this).parents(".dashlayout-sidebar-menu-item").index();


        //     setActiveSideMenuItem(selectedIndex);


        // })




    }


    // function setActiveSideMenuItem(index) {

    //     $.map($(".dashlayout-sidebar-menu-item"), function(n) {


    //         $(n).toggleClass("is-active", $(n).index() == index);


    //     })



    // }

    function stickyLogoSticky() {


        $(window).on("scroll debouncedresize", function() {
            if (viewport().width >= 1024) {


                updateStickyLogo();



            } else {

                // $(window).off("scroll.dashboard");
                $(".dashheader-logo-inner-black-inner").height("auto");

            }

        });


    }


    function initSidebarSicky() {


        $(".dashlayout-sidebar-menu").stick_in_parent({

            offset_top: function() {
                if (viewport().width >= 1024) {
                    return 104;
                } else {
                    return 0
                }
            }

        });




        // $('.dashlayout-sidebar-menu-sticky').waypoint('sticky', {
        //     stuckClass: 'is-stuck',
        //     offset: function() {

        //         if (viewport().width >= 1024) {
        //             return 104;
        //         } else {
        //             return 0
        //         }
        //     },
        //     hasWrapper: true
        // });

        $("html").addClass("has-enoughroomforsticky")

        updateStickyLogo();
    }


    function updateStickyLogo() {

        var scrollTop = $(window).scrollTop();

        var $sidebar = $(".dashlayout-sidebar")

        var sidebarOffsetTop = $(".dashlayout-sidebar").offset().top;

        var sidebarHeight = $(".dashlayout-sidebar").outerHeight();

        $(".dashheader-logo-inner-black-inner").height(sidebarOffsetTop - $(window).scrollTop() - (17));


        $(".dashheader-logo-inner-background-sidebar").height(sidebarOffsetTop + sidebarHeight - $(window).scrollTop())

        $(".dashheader-logo-inner-background").toggle(scrollTop >= 158)



        if (scrollTop + $(window).height() < $(".dashlayout-sidebar").offset().top + sidebarHeight) {
            $("html").addClass("has-enoughroomforconsultantsticky")

        } else {
            $("html").removeClass("has-enoughroomforconsultantsticky")
        }



    }

    function canFitSticky() {

        return ($(window).height() > ($(".dashheader").height() + $(".dashaccountcurrentbilling").height() + $(".dashlayout-sidebar-menu-list").height()))

    }

    function initDashaccountcurrentbillingSwitch() {

        $('.dashaccountcurrentbilling-switch select').selectOrDie({
            customClass: "sod_select_account"
            // ,
            // onChange:function(){


            //   $(this).parents(".sod_select").toggleClass("is-placeholder",$(this).val()==""||$(this).val()=="Select")

            // }
        });

        // $(".dashaccountcurrentbilling-switch .sod_select").addClass("is-placeholder");



    }


    function initMobileNav() {


        $(".dashheader-toggle").on(Modernizr.touchevents ? "touchstart" : "click", function() {

            $(".dashheader").toggleClass("is-headermenu-open");

        });

    }



    return {
        init: init
    }
}(window.jQuery));
