App.dashaccount = (function($) {
    "use strict";
    var _Initialised = false;
    var didValidate = false;

    var oldSize;

    var $animationDiv;

    var numberOfLinkedAccounts = 0;



    function init() {
        if (!_Initialised && $(".dashaccount").length) {
            _Initialised = true;
            initEvents();


        }
    }




    function initEvents() {



        $(".dashaccount-addsecondary").on("click",function(){

            App.popupeditform.openPopup('.popup--secondarycontactadd',this)

            $(".dashaccount-addsecondary").hide();

            $('.dashaccount-section--secondary').show();



        });





       $(".dashaccount-account-postal-tools-item.is-edit").on("click",function(){

            var $parent = $(this).parents(".dashaccount-account-postal");

            App.form.copyValuesFromFormToForm($parent, $(".popup--postaladdress"))


                if(App.dashaccount.beforePostalAddressOpen){

                    App.dashaccount.beforePostalAddressOpen();

                }


                App.popupeditform.openPopup('.popup--postaladdress',$(this).closest(".dashaccount-account-postal")[0])


        })



       $(".dashaccount-section--login .dashaccount-section-form-tools-item.is-edit").on("click",function(){



            App.form.copyValuesFromFormToForm($(".dashaccount-section--login"), $(".popup--logindetails"))




                App.popupeditform.openPopup('.popup--logindetails',$(this).closest(".dashaccount-section-form")[0])


        })


       $(".dashaccount-section--primary .dashaccount-section-form-tools-item.is-edit").on("click",function(){


                App.popupeditform.openPopup('.popup--primarycontact',$(this).closest(".dashaccount-section-form")[0])

        })



       $(".dashaccount-section--secondary .dashaccount-section-form-tools-item.is-edit").on("click",function(){

                App.popupeditform.openPopup('.popup--secondarycontactedit',$(this).closest(".dashaccount-section-form")[0])

        })


    }



    return {
        init: init
    }
}(window.jQuery));
