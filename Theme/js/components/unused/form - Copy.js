/*global NBNUI  */
App.form = (function($) {
    "use strict";
    var _Initialised = false;
    var _adjusterTimer;
    var _buttonLoadingTimeout;
    var $parent;
    // var checkSVG = '<svg class="form-check-svg" version="1.1" x="0px" y="0px" viewBox="0 0 28.3 23.1" enable-background="new 0 0 28.3 23.1" xml:space="preserve"><path fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#5CC5C4" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M1.5,14.5c0,0,5.4,3.7,7.1,6c0,0,12.8-18.3,18.2-19" stroke-dasharray="35.9658" stroke-dashoffset="35.96581"/></svg>';
    function init($parent) {
        if (!$parent) {
            $parent = $("body");
        }
            // if (!_Initialised) {
        _Initialised = true;
        initFields();
        initCountDown();
        initMultipleUpload();
        initImageCheckbox();
        initDate();
    }

    function initDate() {
        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        if (!iOS) {
            $parent.find("input[type=date]").pickadate({
                weekdaysShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
                selectMonths: true,
                selectYears: true,
                today: '',
                clear: '',
                close: '',
                format: 'dd/mm/yyyy',
                formatSubmit: 'yyyy-mm-dd',
                firstDay: 1,
                hiddenName: true
            })
        }
    }

    function initImageCheckbox() {
        $parent.find(".js-toggle-all-checkbox").change(function() {
            if (this.checked) {
                $(".js-imagecheckbox-list input:checkbox").prop("checked", true);
            } else {
                $(".js-imagecheckbox-list input:checkbox").prop("checked", false);
            }
        });
        $parent.find(".js-imagecheckbox-list input").change(function() {
            var AllChecked = ($(".js-imagecheckbox-list input:checked").length == $(".js-imagecheckbox-list input").length)
            $(".js-toggle-all-checkbox").prop("checked", AllChecked)
        });
    }

    function initCountDown() {
        $parent.find("textarea[maxlength]").each(function() {
            var theMaxLength = parseInt($(this).attr("maxlength"));
            var theRow = $(this).closest(".form-row");
            theRow.find(".form-field-countdown-total").text(theMaxLength);
            $(this).on("keydown keyup", function() {
                var theValue = $(this).val();
                var thelength = theValue.length;
                if (thelength > theMaxLength) {
                    theValue = theValue.substring(0, theMaxLength);
                    $(this).val(theValue)
                }
                var thelength = theValue.length;
                $(".form-field-countdown-current").text(thelength)
            });
        })
    }

    function initMultipleUpload() {
        $parent.find(".form-upload input").on("change", function(e) {
            var $element = $(this);
            var $control = $(this).closest(".form-upload");
            var $set = $element.closest(".form-multipleupload");
            var $row = $set.closest(".form-row");
            var $validation = $row.find(".form-validation");
            var $validationMessage = $row.find(".form-validation-message");
            var fileSize = 0;
            if (this.files && this.files[0]) {
                fileSize = this.files[0].size;
            }
            var uploadValidation = checkForValidFile(this.value, fileSize)
            if (uploadValidation == true) {
                $control.attr("completed", true);
                $validation.hide();
                $row.removeClass("is-invalid")
            } else {
                $control.attr("completed", false);
                $element.replaceWith($element.val('').clone(true));
                $row.removeClass("is-valid")
                $row.addClass("is-invalid")
                $validationMessage.text(uploadValidation);
                $validation.show();
            }
            processUploadDisplays($set);
        });
        $parent.find(".form-multipleupload-display-delete").on("click", function() {
            var $set = $(this).closest(".form-multipleupload");
            var displayItemIndex = $(this).closest(".form-multipleupload-display-item").index();
            var $input = $set.find("input").eq(displayItemIndex);
            var $control = $set.find(".form-upload").eq(displayItemIndex);
            $control.attr("completed", false);
            $input.replaceWith($input.val('').clone(true));
            processUploadDisplays($set)
        });

        function checkForValidFile(fileName, size) {
            var validExtension = false;
            var validSize = false;
            var maxFileSize = 3 * 1000 * 1000; // 1MB
            // Use a regular expression to trim everything before final dot
            var extension = fileName.replace(/^.*\./, '');
            // Iff there is no dot anywhere in filename, we would have extension == filename,
            // so we account for this possibility now
            if (extension == fileName) {
                extension = '';
            } else {
                // if there is an extension, we convert to lower case
                // (N.B. this conversion will not effect the value of the extension
                // on the file upload.)
                extension = extension.toLowerCase();
            }
            var bannedExtentions = ".ade, .adp, .bat, .chm, .cmd, .com, .cpl, .exe, .hta, .ins, .isp, .jar, .jse, .lib, .lnk, .mde, .msc, .msp, .mst, .pif, .scr, .sct, .shb, .sys, .vb, .vbe, .vbs, .vxd, .wsc, .wsf, .wsh";
            if (bannedExtentions.indexOf(extension) == -1) {
                validExtension = true;
            }
            if (!validExtension) {
                return "Sorry, files with the extention ." + extension + " are not allowed."
            }
            if (size <= maxFileSize) {
                validSize = true
                console.log(size, maxFileSize)
            } else {
                return "Sorry, the file is too large (" + (Math.round(size / 1000 / 1000 * 10) / 10) + "MB). 3MB is the file size limit."
            }
            return validExtension && validSize;
        }

        function processUploadDisplays($set) {
            var $uploaderItems = $set.find(".form-multipleupload-uploaders-item")
            var $displayItems = $set.find(".form-multipleupload-display-item")
            var numberOfControls = $uploaderItems.length;
            var isCompleted;
            var nextUploadIndex = -1;
            for (var i = 0; i < $displayItems.length; i++) {
                isCompleted = $uploaderItems.eq(i).find(".form-upload").attr("completed");
                if (isCompleted === "true") {
                    var path = $uploaderItems.eq(i).find("input").val();
                    var fileName = path.replace(/^.*\\/, "");
                    $displayItems.eq(i).find(".form-multipleupload-display-copy").text(fileName);
                    $uploaderItems.eq(i).addClass("is-active");
                    $displayItems.eq(i).addClass("is-active");
                } else {
                    //get next available
                    if (nextUploadIndex == -1) {
                        nextUploadIndex = i;
                    }
                    $uploaderItems.eq(i).removeClass("is-active");
                    $displayItems.eq(i).removeClass("is-active");
                }
            };
            if (nextUploadIndex != -1) {
                $uploaderItems.not(":eq(" + nextUploadIndex + ")").removeClass("is-active");
                $uploaderItems.eq(nextUploadIndex).addClass("is-active");
            } else {
                $uploaderItems.removeClass("is-active");
            }
        }
    }

    function initAnimatedCheck() {
        // var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        // var is_safari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;
        // var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
        // if(is_chrome || is_safari || is_firefox){
        //   $(".form-check-label").before($(checkSVG))
        // }
    }

    function resetForm($element) {
        $element = $($element);
        $element.find("input, select").filter(":not(:button):not(:submit):not([type=checkbox]):not([type=radio])").removeClass("is-invalid").val("").change();
        $element.find("[type=checkbox], [type=radio]").removeClass("is-invalid").prop("checked", false).change();
        $element.find(".form-validation-message").empty();
        $element.find(".form-row.is-invalid").removeClass("is-invalid");
    }

    function initFields() {

        $parent.find('.form select').each(function() {
            var $this = $(this);
            if ($this.hasClass("is-inlinelabel")) {
                console.log
                $this.selectOrDie({
                    prefix: $this.prev().text() + ":",
                    customClass: "sod_select_default"
                });
            } else {
                $this.selectOrDie({
                    customClass: "sod_select_default"
                });
            }
        });
        $parent.find('.sod_select_theme_naturalinline').selectOrDie({
            customClass: "sod_select_naturalinline",
            onChange: function() {
                $(this).parents(".sod_select").toggleClass("is-placeholder", $(this).val() == "" || $(this).val() == "Select")
            }
        });
        initExpandable();
    }

    function initExpandable() {
        if ($(".js-form-expandable").length) {
            console.log("initExpandable");
            var $disableElement = $('.js-toggle-enable');
            $parent.find(".js-form-expandable-toggle").click(function() {
                //var theParent = $(this).parents(".js-form-expandable");
                var theParent = $(this).closest(".filter-control").find(".js-form-expandable-content");
                //.find(".js-form-expandable")
                if ($(this).hasClass("is-form-expandable-open")) {
                    $(this).removeClass("is-form-expandable-open");
                    $disableElement.addClass("disable")
                    $("select.js-toggle-enable").selectOrDie("enable");
                    $disableElement.prop('disabled', false)
                    $(theParent).slideUp(400);
                } else {
                    $(this).addClass("is-form-expandable-open");
                    $disableElement.removeClass("disable")
                    $("select.js-toggle-enable").selectOrDie("disable");
                    $disableElement.prop('disabled', true);
                    $(theParent).slideDown(400);
                }
            });
            $parent.find(".js-form-expandable-toggle").click(function() {
                $disableElement.toggleClass("disable")
            });
        }
    }
    return {
        init: init,
        // toggleProminentEqualHeight: toggleProminentEqualHeight,
        // copyValuesFromFormToForm: copyValuesFromFormToForm,
        resetForm: resetForm
            // ,
            // setButtonToLoading: setButtonToLoading,
            // setButtonToFinishedLoading: setButtonToFinishedLoading
            // initFieldStates: initFieldStates,
            // setRangeValue: setRangeValue,
            // setAdjusterStep: setAdjusterStep
    };
})(jQuery);
