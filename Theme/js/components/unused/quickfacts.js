App.quickfacts = (function($) {
  "use strict";
  var _Initialised = false;
  var _hasPlayed = false;

  function init() {
    if (!_Initialised && $(".quickfacts").length) {
      _Initialised = true;
      initEvents();
    }
  }

  function initEvents() {

      setIsOverflow();
      expandContent();

      $(window).on("debouncedresize", function() {
        setIsOverflow();
        expandContent()
      })

      $(window).on("load", function() {

      })




  }


  function setIsOverflow(){
    var $item = $(".quickfacts-item");
    var $itemcontentInner = $item.find(".quickfacts-item-content-inner");


    $itemcontentInner.each( function() {
        var itemcontentHeight = $(this).height();
        itemcontentHeight = parseInt(itemcontentHeight);

        if(viewport>1024){
          if(itemcontentHeight > 340){
            $(this).closest(".quickfacts-item").addClass("is-overflow")
          }
          else
            $(this).closest(".quickfacts-item").removeClass("is-overflow")
        }

        else{
          if(itemcontentHeight > 200){
            $(this).closest(".quickfacts-item").addClass("is-overflow")
          }
          else
            $(this).closest(".quickfacts-item").removeClass("is-overflow")
        }




    });

  }

  function expandContent(){
       $(".quickfacts-button, .quickfacts-content").on("click",function(e){
            e.preventDefault();
            console.log(e.preventDefault)

            var $theItem = $(this).closest(".quickfacts-item");

            if(!$theItem.hasClass("is-open")){

              $theItem.addClass("is-open");

              var itemcontentHeight = $theItem.find(".quickfacts-item-content-inner").height();
              var $itemcontent =  $theItem.find(".quickfacts-item-content");
              $itemcontent.height(itemcontentHeight);
              var $itembutton  = $theItem.find(".quickfacts-item-button-wrap");
              $itembutton.slideUp();

            }

       })
  }


  return {
    init: init
  }
}(window.jQuery));
