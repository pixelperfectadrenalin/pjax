App.accordion = (function($) {
	"use strict";

	function init() {
		if ($(".accordion-reed").length) {
		//	initCategories();
			initReeds();
		}
	}

	// function initCategories() {
	// 	$('.accordion-category').openClose({
	// 		activeClass: 'is-accordion-category-open',
	// 		opener: '.accordion-category-title',
	// 		slider: '.accordion-category-content',
	// 		animSpeed: 400,
	// 		effect: 'slide'
	// 	});
	// }

	function initReeds() {
		
		$('.accordion-reed').openClose({
			activeClass: 'is-accordion-reed-open',
			opener: '.accordion-reed-title',
			slider: '.accordion-reed-content',
			animSpeed: 400,
			effect: 'slide'
		});
	}
	return {
		init: init
	}
}(window.jQuery));
