App.detailtimeline = (function($) {
  "use strict";
  var _Initialised = false;
  var _hasPlayed = false;

  function init() {
    if (!_Initialised && $(".detailtimeline").length) {
      _Initialised = true;
      initEvents();
    }
  }

  function initEvents() {


            initCarousel();

            // $(window).on("debouncedresize", function() {
            //   App.reviews.handleResize();
            // })

            // $(window).on("load", function() {
            //   App.reviews.handleResize();
            // })




  }

  function initCarousel(){





    $(".detailtimeline-list").slick({
      mobileFirst:true,
      focusOnSelect: true,
      // autoplay: true,
      // autoplaySpeed: 8000,
      centerMode: false,
      adaptiveHeight: false,
      infinite: false,
      dots: false,
      draggable:true,
      slidesToShow: 4,
      slidesToScroll: 1,
      speed: 300,
      touchThreshold: 100,
      useCSS: true,
      variableWidth: false,
      arrows: true,
      // fade: true,
      vertical:true,
      responsive: [{
        breakpoint: 1023,
        settings: {
          vertical:false,
          slidesToShow: 6,
          slidesToScroll: 1
        }
      }]
    });


  }


  // function handleResize(){


  //     initCarousel();

  // }


  return {
    init: init
    // ,
    // handleResize: handleResize
  }
}(window.jQuery));
