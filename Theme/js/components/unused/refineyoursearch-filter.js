App.refineyoursearchaccordion = (function($) {
	"use strict";

	function init() {
		if ($(".refineyoursearch-filter").length) {
			initCategories();
		}
	}

	function initCategories() {
		$('.refineyoursearch-filter-section-reed').openClose({
			activeClass: 'is-accordion-reed-open',
			opener: '.refineyoursearch-filter-section-reed-title',
			slider: '.refineyoursearch-filter-section-reed-content',
			animSpeed: 400,
			effect: 'slide'
		});
	}

	
	return {
		init: init
	}
}(window.jQuery));
