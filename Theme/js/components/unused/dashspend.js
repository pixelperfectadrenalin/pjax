App.dashspend = (function($) {
    "use strict";
    var _Initialised = false;
    var didValidate = false;
    var oldSize;
    var $animationDiv;

    function init() {
        if (!_Initialised && $(".dashspend").length) {
            _Initialised = true;
            initEvents();
            $animationDiv = $("<div>");
        }
    }

    function initEvents() {
        initPreviousBills();
        initSpendEvents();
        // initUsageEvents()
        initQuickTourEvents();
    }

    function initSpendEvents() {
        loadSpendData();
        $(".dashspend-lastupdated-icon").on("click", function() {
            loadSpendData();
        })
        $("#dashspend-currentvsprojectedswitch, #dashspend-solarsavingsswitch").on("change", function() {
            loadSpendData();
        })
    }



    function initQuickTourEvents() {

        if($("#quicktourautoshow").val()=="true"){
              openQuickTour();
        }

        $(".dashspend-helpbutton").on("click", function() {
            openQuickTour();
        });
    }

    function openQuickTour(){

            App.popupdash.openPopup('.popup-dash--quicktour', function() {
                initQuicktourCarousel();
            }, function() {
                removeQuicktourCarousel();
            },$(".dashspend-helpbutton")[0])


    }

    function loadSpendData() {
        // var currentOrProjected = ($("#dashspend-currentvsprojectedswitch").prop("checked")) ? "current" : "projected"
        // var solar = ($("#dashspend-solarsavingsswitch").prop("checked")) ? "Solar" : "NoSolar"

        var currentOrProjected = "current";

        // var currentOrProjected = "current";
        $.ajax({
            dataType: "json",
            url: $("#spendcharturl").val(),
            error: function(data) {
                console.log("Error", data)
            },
            success: function(data) {
                // var theState = data.states[currentOrProjected + solar];
                var theState = data.states["initial"];
                var spendPercentage = theState.spend / theState.total
                var overPercentage = 0;
                if (theState.spend > theState.total) {
                    spendPercentage = 1;
                    overPercentage = 1;
                }
                $animationDiv.stop().css("left", 0).animate({
                    "left": 100
                }, {
                    step: function(now, fn) {
                        App.svganimate.fill($("#dashspend-spendchart-spend .dashspend-spendchart-spend")[0], 0, now * spendPercentage * .88);
                        App.svganimate.fill($("#dashspend-spendchart-spend .dashspend-spendchart-over")[0], 0, now * overPercentage * .95);
                        $(".dashspend-spendchart-estimate").css({
                            "transform": "rotate(" + (now / 100 * 360 * 0.88 * theState.estimate / theState.total) + "deg)"
                        }, {
                            duration: 3000
                        });
                    },
                        duration: 3000

                })
                $(".dashspend-spendchart-label-maximum").text("$" + theState.total)
                $(".dashspend-spendchart-content-value").text("$" + theState.spend)
                $(".dashspend-spendchart-content-description").text("$" + theState.spend)
                $(".dashspend-spendchart-content-description").text("for " + theState.kilowatthours + " kWh usage")
                if (currentOrProjected == "current") {
                    $(".dashspend-title").text("Current Spend");
                    $(".dashspend-spendchart-content-day-current").text("DAY " + data.currentDayInBillingPeriod);
                    $(".dashspend-spendchart-content-day-total").text("/ " + data.totalNumberOfDaysInBillingPeriod).show();
                } else if (currentOrProjected == "projected") {
                    $(".dashspend-title").text("Projected Spend");
                    $(".dashspend-spendchart-content-day-current").text("DAY " + data.totalNumberOfDaysInBillingPeriod);
                    $(".dashspend-spendchart-content-day-total").hide();
                }
                // $(".dashspend-lastupdated-copy").text("Last updated on "+moment().format('DD/MM/YYYY [at] h:mm a'))
                $(".dashspend-lastupdated-copy").text("Last updated on " + data.updateddatetime)
            }
        });
    }

    function initPreviousBills() {
        $(".dashspend-previousbills-months-list").slick({
            dots: false,
            arrows: true,
            infinite: false,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            touchThreshold: 100,
        });
    }

    function initQuicktourCarousel() {
        $(".popup-dash-quicktour-list").slick({
            dots: true,
            arrows: true,
            infinite: false,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            touchThreshold: 100,
            adaptiveHeight: true,
            // appendArrows:".popup-dash-controls-inner"
              // responsive: [
              //   {
              //     breakpoint: 767,
              //     settings: {
              //       adaptiveHeight: true,
              //       // centerPadding: '18%',
              //       // slidesToShow: 1,
              //       // slidesToScroll: 1
              //     }
              //   }
              // ]

        });
    }

    function removeQuicktourCarousel() {
        $(".popup-dash-quicktour-list").slick("unslick")
    }


    return {
        init: init
    }
}(window.jQuery));
