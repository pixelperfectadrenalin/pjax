/*global App  */
App.svganimate = (function($) {
  "use strict";
  var _Initialised = false;

  function init() {
    if (!_Initialised) {
      _Initialised = true;
      // initAnimationFramePolyfill();

      $("svg").each(function(index, element){


        // initAnimationFramePolyfill();

        // initPaths(element);

      });



    }
  }





  // function initAnimationFramePolyfill(){


  // }


  function initPaths(svg){

    $(svg).find("path").each(function(index, element){

      var $element = $(element);

      var length = $element[0].getTotalLength();
      $element.data("length",length);
      $element.css("stroke-dasharray", length + ' ' + length);
      $element.css("stroke-dashoffset", length*-1);

    });


  }


    function play(element, callback){

      var $element = $(element);

      var $elements;

      if($element.is("svg")){

        $elements = $element.find("path")

      }else{
        $elements = $element;
      }


      $elements.each(function(index, element){

        var $element = $(element);

        var length = $element.data("length");

        $element.hide().show().animate({"stroke-dashoffset":0},500);

      });


    }



    function fill(element, start, end){

      // var $element = $(element;


      var length = element.getTotalLength();


      var start = (start/100)*length;

      var end = (end/100)*length;


      var difference = (end-start);

      var offset


      // var gap = (gap/100)*length;

      var gap = (length-difference);




      window.requestAnimationFrame( function(){


          var redrawText = document.createTextNode(" ")



            element.style.strokeDasharray = difference + ' ' + gap;
            element.style.strokeDashoffset = start + difference;



            element.appendChild(redrawText);


            element.removeChild(redrawText);


      })



    }









  // $("<div>").animate({"left":100},{step:function(now, fn){


  //       App.svganimate.fill("#chart1 .chart-fill", 0, now);

  //   }})



    // function play(element, callback){

    //   var $element = $(element);

    //   var $elements;

    //   if($element.is("svg")){

    //     $elements = $element.find("path")

    //   }else{
    //     $elements = $element;
    //   }


    //   $elements.each(function(index, element){

    //     var $element = $(element);

    //     $element.data("currentframe",0);

    //     nextFrame($element);

    //   });


    // }



    // function nextFrame($element){

    //     var currentframe = $element.data("currentframe");

    //     var handle = $element.data("handle");

    //     var length = $element.data("length");

    //     var totalframes = 60;

    //     var progress = currentframe/totalframes;


    //     if (progress > 1) {
    //       window.cancelAnimFrame(handle);

    //       // callback();

    //     } else {
    //     currentframe++;
    //     $element.data("currentframe",currentframe);

    //       // for(var j=0, len = this.path.length; j<len;j++){
    //         $element.css("stroke-dashoffset",Math.floor(length * (1 - progress)*-1));
    //       // }

    //       $element.data("handle",window.requestAnimFrame(function() { nextFrame($element) }))
    //     }



    // }



  return {
    init: init,
    play: play,
    fill: fill
  };
}(window.jQuery));
