App.dashrefer = (function($) {
    "use strict";
    var _Initialised = false;


    function init() {
        if (!_Initialised && $(".dashrefer").length) {
            _Initialised = true;
            initEvents();


        }
    }




    function initEvents() {

      $("#referralurl").on("click",function(){
           $(this)[0].focus();
           this.setSelectionRange(0, 9999);
      })

    }



    return {
        init: init
    }
}(window.jQuery));
