App.reviews = (function($) {
  "use strict";
  var _Initialised = false;
  var _hasPlayed = false;

  function init() {
    if (!_Initialised && $(".reviews-list").length) {
      _Initialised = true;
      initEvents();
    }
  }

  function initEvents() {


            equalHeight();

            $(window).on("debouncedresize", function() {
              App.reviews.equalHeight();
            })

            $(window).on("load", function() {
              App.reviews.equalHeight();
            })




  }


  function equalHeight(){

    App.equalHeight.equalHeight({elements:".reviews-item-content"});

  }


  return {
    init: init,
    equalHeight: equalHeight
  }
}(window.jQuery));
