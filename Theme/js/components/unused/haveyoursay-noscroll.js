App.haveyoursay = (function($) {
  "use strict";
  var _Initialised = false;
  var _hasPlayed = false;

  var scrollTop;


  function init() {
    if (!_Initialised && $(".haveyoursay").length) {
      _Initialised = true;
      initEvents();
    }
  }

  function initEvents() {


    $(".haveyoursay-link-button").on("click",function(e){

        e.preventDefault();

        openPopup();


    });

    $(".haveyoursay-popup-button").on("click",function(){


                $.magnificPopup.close();


    });




  }



function openPopup(){



                $.magnificPopup.open({

                    // prependTo: $("form:first"),
                    // delegate: 'a',
                    items: {
                        src: $(".haveyoursay-popup"),
                        type: 'inline'
                    },
                    // tLoading: 'Loading image #%curr%...',
                    mainClass: 'popup-haveyoursay-outer mfp-slidefromright',


                    closeBtnInside: true,
                    preloader: false,
                    removalDelay: 400, // Delay to allow for fade out


                    callbacks: {

                    // fixedContentPos:false,
                    // fixedBgPos:false,


                    open:function(){


                          scrollTop = $("body").scrollTop();




                              onResize()

                          $(window).on("debouncedresize.popup", function() {

                              onResize()

                          });



                    },

                    afterClose:function(){

                            $("html").removeClass("mfp-popupopen");
                            // $("html").css({"overflow-x":"auto"});
                            $("#page").css({"margin-top":0});
                            $("#page").css({"height":"auto"});
                            $("html").css({"overflow":"auto"});
                            $("#page").css({"margin-bottom":0});

                            $("body").scrollTop(scrollTop);


                          $(window).off("debouncedresize.popup");




                    },


                    }

                });

}





function onResize(){

                          var overflowHeight;

                          var $popup = $(".haveyoursay-popup");


                          var $popupOuterBg = $(".popup-haveyoursay-outer.mfp-bg");

                          var $popupOuterWrap = $(".popup-haveyoursay-outer.mfp-wrap");

                          var $body = $("body");

                          var $window = $(window);

                          var $page = $("#page");

                          var $html = $("html");

                          var popupHeight = $popup.height();


                          var windowHeight = $window.height();



console.log("popupHeight", popupHeight);

console.log("windowHeight", windowHeight);

                            if(popupHeight > windowHeight){
                              overflowHeight = popupHeight
                              $html.css({"margin-right":"0"});
                              $(".mfp-content").css({"padding-right":"0"});

                            }else{
                              overflowHeight = windowHeight
                            }



                            $page.css({"overflow":"hidden"});
                            $page.css({"height":overflowHeight +scrollTop}); // +
                            $page.css({"margin-top":scrollTop *-1});
                            // $page.css({"margin-bottom":scrollTop *-1});
                            $body.scrollTop(0);
                            $popupOuterWrap.css({"top":0})
                            $popupOuterWrap.css({"height":overflowHeight})
                            $popupOuterWrap.css({"position":"absolute"});

                            $popupOuterBg.css({"position":"fixed"});

}






  return {
    init: init
  }
}(window.jQuery));
