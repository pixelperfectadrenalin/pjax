/*global App  */
App.formValidate = (function($) {
    "use strict";
    var _Initialised = false;
    var _validator;
    var _dynamicallyGeneratedForm;



    function init() {




        if (!_Initialised && $(".js-form--validate").length) {
            _Initialised = true;
            addValidationMethods();
            addValidationClassRules();
            _validator = $("form").each(function(index, Element){
              $(Element).validateWebForm({
                debug: true,
                errorClass: "is-invalid",
                validClass: "is-valid",
                blurClass: "is-blured",
                focusClass: "is-focused",

                focusInvalid: false,
                submitHandler: function (form) {
                    // form.submit();
                    if($(form).valid()){
                      form.submit()
                    }else{


                  // if(validator.numberOfInvalids()){
                  // }else{
                  //   $(".form-overviewmessage").stop().slideUp();
                  // }

                    };

                },

                // invalidHandler: function(form, validator) {

                //     if (!validator.numberOfInvalids())
                //         return;

                //       console.log($(validator.errorList[0].element));

                //     $('html, body').animate({
                //         scrollTop: $(validator.errorList[0].element).parents(".form-row").offset().top
                //     }, 1000, function() {

                //         $(validator.errorList[0].element).focus();

                //     });

                // },

                invalidHandler: function(form, validator) {



                    if (!validator.numberOfInvalids())
                        return;

                    var $errorElement = $(validator.errorList[0].element);
                    console.log($errorElement)


                    var $scrollParent = $errorElement.scrollParent();

                    var scrollToTop;

                    if(!$scrollParent.prop("tagName")){

                      $scrollParent = $("html, body");
                      scrollToTop = $errorElement.parents(".form-row").offset().top;// - $scrollParent.offset().top;

                    }else{

                      scrollToTop = $errorElement.parents(".form-row").position().top;// - $scrollParent.offset().top;

                    }


                    $scrollParent.animate({
                        scrollTop: scrollToTop
                    }, 1000, function() {

                    if(validator.errorList.length){

                        $(validator.errorList[0].element).focus();

                          }
                    });

                },



                onfocusin: function(element) {
                    this.lastActive = element;
                    // hide error label and remove error class on focus if enabled
                    if (this.settings.focusCleanup && !this.blockFocusCleanup) {
                        if (this.settings.unhighlight) {
                            this.settings.unhighlight.call(this, element, this.settings
                                .errorClass, this.settings.validClass);
                        }
                        this.addWrapper(this.errorsFor(element)).hide();
                    }
                    element = $(element);
                    if (element.prop("readonly")) {
                        return true
                    }
                    var elementValue = element.val();
                    var $theRow = element.parents(".form-row");
                    var focusClass = this.settings.focusClass;
                    var blurClass = this.settings.blurClass;
                    $theRow.removeClass(blurClass);
                    $theRow.addClass(focusClass);
                    element.removeClass(blurClass);
                    element.addClass(focusClass);
                },
                onfocusout: function(element) {
                    if (!this.checkable(element) && (element.name in this.submitted ||
                        !this.optional(element))) {
                        this.element(element);
                    }
                    element = $(element);
                    if (element.prop("readonly")) {
                        return true
                    }
                    var elementValue = element.val();
                    var $theRow = element.parents(".form-row");
                    var focusClass = this.settings.focusClass;
                    var blurClass = this.settings.blurClass;
                    $theRow.removeClass(focusClass);
                    $theRow.addClass(blurClass);
                    element.removeClass(focusClass);
                    element.addClass(blurClass);
                },
                unhighlight: function(element, errorClass, validClass) {
                    element = $(element);
                    var elementValue = element.val();
                    var $theRow = element.parents(".form-row");
                    var $theValidation = $theRow.find(".form-validation");
                    $theRow.removeClass(errorClass);
                    element.removeClass(errorClass);
                    element.addClass(validClass);
                    $theRow.addClass(validClass);
                    //If the value is valid (we know it is because we're in "unhighlight"), AND blank, ignore it, as a tick next to a blank field looks odd.
                    //A blank SELECT.val() produces null, so catch case this too
                    // if (typeof elementValue == "string" && elementValue !== "" && !$theRow.hasClass("form-row-suppressvalid")) {
                    //     $theValidation.show();
                    // } else {
                    //     $theValidation.hide();
                    // }
                    //suppress all success
                    $theValidation.hide();
                },
                highlight: function(element, errorClass, validClass) {
                    element = $(element);
                    if (element.is(":hidden")) {
                        return false;
                    }
                    var $theRow = element.parents(".form-row");
                    var $theValidation = $theRow.find(".form-validation");

                    $theRow.removeClass(validClass);
                    $theRow.addClass(errorClass);
                    element.removeClass(validClass);
                    element.addClass(errorClass);
                    $theValidation.show();
                },
                errorPlacement: function(error, element) {
                    element = $(element);
                    var $theRow = element.parents(".form-row");
                    var $theValidationMessage = $theRow.find(".form-validation-message");
                    error.appendTo($theValidationMessage);
                    // }
                }
            });

          });


            //VALIDATE SELECT WHEN CHANGED
            $(".js-form--validate").on('change', 'select', function() {
                $(this).valid();
            });
            //VALIDATE RANGE WHEN CHANGED
            $(".js-form--validate").on('change', '.form-slider input', function() {
                $(this).valid();
            });
        }
    }

    function addValidationMethods() {


        $.validator.addMethod('validation_password', function(value, element) {
            return this.optional(element) || value.length >= 8 && /[A-Z]/.test(value) && /[0-9]/.test(value);
        }, 'Password must be a minimum 8 characters, and contain 1 uppercase letter and 1 number.');



        $.validator.addMethod('validation_confirmpassword', function(value, element) {
            return this.optional(element) || value == $("#password").val();
        }, 'Passwords do not match.');


        $.validator.addMethod('validation_confirmemail', function(value, element) {
            return this.optional(element) || value == $("#email").val();
        }, 'Please confirm your email address.');



        $.validator.addMethod('required', $.validator.methods.required, 'This is a required field.');

        // Browsers that support date properly should never get here as an invalid date should read .val() as blank "" instead
        $.validator.addMethod('validation_date', function(value, element) {
            return this.optional(element) || /^\d{4}-\d{2}-\d{2}$/.test(value);
        },  'Please enter a date in yyyy-mm-dd format.');


        $.validator.addMethod('validation_ccnumber_required', $.validator.methods.required,
            'Please enter your credit card number');
        $.validator.addMethod('validation_ccnumber_valid', function(value, element) {
           var isPlaceholder = value.indexOf("XXXX-XXXX-XXXX") == 0;
            var ccnumber_result = $(element).validateCreditCard({
                // accept: ['amex', 'diners_club_carte_blanche', 'diners_club_international', 'mastercard', 'visa', 'visa_electron']
                accept: ['amex', 'mastercard', 'visa', 'visa_electron']
            }); //console.log(ccnumber_result.card_type.name);
            App.formCreditcard.highlightCard(ccnumber_result);
            return ccnumber_result.luhn_valid && ccnumber_result.length_valid || isPlaceholder;
        }, 'Please enter a valid credit card number. ');
        $.validator.addMethod('validation_ccnumberlength', function(value, element) {
           var isPlaceholder = value.indexOf("XXXX-XXXX-XXXX") == 0;
            var ccnumber_result = $(element).validateCreditCard({
                // accept: ['amex', 'diners_club_carte_blanche', 'diners_club_international', 'mastercard', 'visa', 'visa_electron']
                accept: ['amex', 'mastercard', 'visa', 'visa_electron']
            });
            //console.log(ccnumber_result.card_type.name);
            App.formCreditcard.highlightCard(ccnumber_result);
            return ccnumber_result.length_valid || isPlaceholder;
        }, 'Please check the number of digits entered');
        $.validator.addMethod('validation_ccname', $.validator.methods.required,
            'Please enter the name on your card. ');
        $.validator.addMethod('validation_ccexpirymonth', $.validator.methods.required,
            'Please select your credit card expiry month. ');
        $.validator.addMethod('validation_ccexpiryyear', $.validator.methods.required,
            'Please select your credit card expiry year. ');
        $.validator.addMethod('validation_cardsecuritycode', $.validator.methods.required,
            'Please enter your credit card CCV. ');
        $.validator.addMethod('validation_number', function(value, element) {
            return this.optional(element) ||
                /^[0-9]+$/
                .test(value);
        }, 'Please enter a number');


        $.validator.addMethod('validation_linkedaccount', function(value, element) {

            // var selectedLinkedAccount = [];

            var $theForm = $(element).closest(".js-form--validate");

            var $linkedAccounts = $theForm.find('.validation_linkedaccount');

            var foundMatch = false;

            console.log("--");
            $linkedAccounts.each(function(){

              if(element != this && $(this).val() === value && $(this).is(":visible")){
              // console.log($(element), $(this), $(this).val() ,value)
                foundMatch = true;
              }
            });


            return !foundMatch


        }, 'Please select an address only once. ');



        $.validator.addMethod('validation_recaptcha', function(value, element) {

            // var selectedLinkedAccount = [];

            var response = grecaptcha.getResponse();

            var recaptchaValid = response!=""

            return recaptchaValid;


        }, 'Please complete the CAPTCHA. ');



    }

    function addValidationClassRules() {
        // Add class rules based on class names
        $.validator.addClassRules({
            required: {
                required: true
            },

            validation_recaptcha:{
              validation_recaptcha:true
            },

            validation_date: {
                validation_date: true
            },

            validation_linkedaccount:{
              validation_linkedaccount:true
            },

            validation_number: {
                validation_number: true
            },
            validation_password: {
                validation_password: true
            },


            validation_confirmemail: {
                validation_confirmemail: true
            },
            validation_confirmpassword: {
                validation_confirmpassword: true
            },

            validation_ccnumber_required: {
                validation_ccnumber_required: true
            },
            validation_ccnumberlength: {
                validation_ccnumberlength: true
            },

            validation_ccname: {
                validation_ccname: true
            },

            validation_ccnumberlength_valid: {
                ccnumberlength_valid: true
            },
            validation_ccexpirymonth: {
                validation_ccexpirymonth: true
            },
            validation_ccexpiryyear: {
                validation_ccexpiryyear: true
            },
            validation_cardsecuritycode: {
                validation_cardsecuritycode: true
            },


        });
    }

    function getValidator() {
        return _validator;
    }

    function resetForm(formElement) {
        var $formElement = $(formElement);
        $formElement[0].reset();
        $(".form-validation", $formElement).hide();
        $('.is-invalid', $formElement).removeClass("is-invalid");
        $('.is-valid', $formElement).removeClass("is-valid");
        $(".form-validation-message", $formElement).empty();
        App.form.initFieldStates();
    }
    return {
        init: init,
        getValidator: getValidator,
        resetForm: resetForm,
        addValidationClassRules: addValidationClassRules
    };
}(window.jQuery));
