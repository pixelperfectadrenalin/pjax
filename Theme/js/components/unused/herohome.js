App.herohome = (function($) {
  "use strict";
  var _Initialised = false;

  var buttonCarousel;

  var debug = 0;

  function init() {
    if (!_Initialised && $(".herohome").length) {
      _Initialised = true;
      initEvents();

    }
  }

  function initEvents() {
      initImages();
      updateCarousel();

  }

  function initImages() {

    App.herohome.handleResize();
    // $(".herohome-slides-item-image-mobile:eq(0), .herohome-slides-item-image-desktop:eq(0)").unveil(0, function() {
    //   finishUnveilingMobileOrDesktop();
    // });





    $(window).on("debouncedresize", function() {
      App.herohome.handleResize();
      // App.herohome.resizeToParent();
    })

    // $(window).on("orientationchange", function() {
    //   _CarouselElement.unslick();
    //   App.herohome.handleResize();
    //   // App.herohome.resizeToParent();
    // })
  }


  function fixForSlickOnResize(){

      if(viewport().width>=1024){

        $('.herohome-buttons-list .slick-track').css('transform', 'translate3d(0px, 0px, 0px)');

      }

  }

  function updateCarousel() {





    $(".herohome-slides-list").slick({
      mobileFirst:true,
      focusOnSelect: true,
      // autoplay: true,
      // autoplaySpeed: 8000,
      dots: false,
      draggable:true,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 300,
      touchThreshold: 100,
      useCSS: true,
      variableWidth: false,
      arrows: true,
      fade: true,
      asNavFor:".herohome-buttons-list",
      appendArrows:".herohome-slides-arrows-inner"

      // ,
      // responsive: [{
      //   breakpoint: 1023,
      //   settings: {
      //     slidesToShow: 1,
      //     slidesToScroll: 1
      //   }
      // }]
    });


    $(".herohome-buttons-list").on("initAfterResponsive reInit",function(){


      fixForSlickOnResize();

      balanceText();
      resizeHero();
      // fitText();

    })


    $(".herohome-buttons-list").on("breakpoint",function(){

      setTimeout(function(){

        fixForSlickOnResize();
        balanceText();

        },300)

    })





    $(".herohome-buttons-list").slick({
      mobileFirst:true,
      focusOnSelect: true,
      adaptiveHeight: false,
     // arrows: false,
      dots: false,
      draggable:true,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 300,
      touchThreshold: 3,
      useCSS: true,
      variableWidth: false,
      arrows: false,
      centerMode:true,
      centerPadding:"20%",
      asNavFor:".herohome-slides-list",

      responsive: [{
        breakpoint: 1023,
        settings: {
          centerMode:false,
          slidesToShow: $(".herohome-buttons-item").length,
          slidesToScroll: 1
        }
      }]
    });



  }


  // 1. slick
  // 2. resize
  // 3. fit text


  // function findheightslider(){
  //   var tallest = 0;
  //   $(".inlinecarousel .inlinecarousel-item").each(function() {
  //     var thisHeight = $(this).height();
  //     if(thisHeight > tallest) {
  //        tallest = thisHeight;
  //     }
  //   });
  //   $(".inlinecarousel-item").height(tallest);


  //   $( window ).resize(function() {
  //     //$(".inlinecarousel .inlinecarousel-item").css("height","auto");
  //     var tallest = 0;
  //     $(".inlinecarousel .inlinecarousel-item").each(function() {
  //       var thisHeight = $(this).height();
  //       if(thisHeight > tallest) {
  //          tallest = thisHeight;
  //       }
  //     });
  //     $(".inlinecarousel .inlinecarousel-item").height(tallest);
  //   });


  // }

  function finishUnveilingMobileOrDesktop() {
    // $(".herohomebackground").waitForImages(function() {
    //   finishedLoadingBackground();
    // });
    // handleResize();
    // App.herohome.resizeToParent();

    //load the rest

  }

  function finishedLoadingBackground() {
    // $(".herohomebackground-inner").css("visibility", "visible");
    // $(".herohomebackground-content").addClass("is-loaded");
  }

  // function resizeToParent() {

  //   // $('.herohomebackground-image-large').resizeToParent();
  //   // $('.herohome-subtiles-item-image-element').resizeToParent();
  // }

  function handleResize() {
    // updateCarousel();
    resizeHero();
    // fitText();
    unveilImages();
    // findheightslider()

  }

  // function fitText() {
  //  $('.herohome-slides-item-content-inner').each(function(){

  //     $(this).textfill({
  //         minFontPixels: 20,
  //         maxFontPixels: 60
  //         ,
  //         callback: function() {
  //           balanceText();
  //         }
  //       });

  //     })
  // }

  function balanceText() {
    //App.balanceText.add($(".herohome-slides-item-content-copy"));

    //App.balanceText.add($(".herohome-buttons-item-inner"));

    // App.balanceText.add($(".herohome-content-subtitle"), {
    //   minimumLines: 1
    // });
  }

  function resizeHero() {
    var windowHeight = viewport().height;
    var windowWidth = viewport().width;

    $(".herohome-slides-carousel").height("auto")


    var headerHeight = $(".header").outerHeight();

    var herohomeArrow = $(".herohome-arrow").outerHeight();

    var buttons = $(".herohome-buttons").outerHeight();


    var maxContentHeight = 0;

    $(".herohome-slides-item-content-inner2").each(function(){

        var theContentHeight = $(this).outerHeight()
        if(theContentHeight>maxContentHeight){
          maxContentHeight = theContentHeight;
        }
    })

    maxContentHeight = maxContentHeight + buttons + herohomeArrow;



    var newHeroHeight  = windowHeight - headerHeight - herohomeArrow;

    if(newHeroHeight<maxContentHeight){
      newHeroHeight = maxContentHeight;
    }

    $(".herohome-slides-carousel").height(newHeroHeight);

    $(".herohome-slides-item-content").css({"padding-bottom":buttons});

    $(".herohome-slides-arrows").css({"margin-top":(buttons/2)*-1});


    // if (viewport().width < 980) {
    //   var herohomebackgroundHeight = $(".herohomecarousel .slick-track").height();
    //   $(".herohomebackground-image-box").height("");
    //   $(".herohomebackground-inner").height("");
    //   $(".herohomecarousel-item").height("");
    //   $(".herohome-content").height(herohomebackgroundHeight);
    // }
    // else {
    //   var navigationHeight = $(".header").height();
    //   var pagerHeight = $(".herohomecarousel-pager").height();
    //   var otherThings = navigationHeight + pagerHeight;
    //   $(".herohome-content").css("padding-top", 0);
    //   $(".herohome-content").height("auto");
    //   var minWindowHeight = 600;
    //   var newWindowHeight = windowHeight;

    //   if (newWindowHeight < minWindowHeight) {
    //     newWindowHeight = minWindowHeight;
    //   }

    //   var newContentHeight = newWindowHeight - otherThings;
    //   $(".herohome-content").height(newContentHeight);
    //   var contentMinHeight = $(".herohome-content").height();
    //   $(".herohomebackground-image-box").height(contentMinHeight + otherThings);
    //   $(".herohomebackground-inner").height(contentMinHeight + otherThings);
    //   $(".herohomecarousel-item").height(contentMinHeight + otherThings);
    // }
  }


  function unveilImages(){
    $(".herohome-slides-item-image-mobile, .herohome-slides-item-image-desktop").unveil(99999999);//huge threashold - hack



  }
  return {
    init: init,
    handleResize: handleResize,
    // resizeToParent: resizeToParent,
    initImages: initImages
  }
}(window.jQuery));
