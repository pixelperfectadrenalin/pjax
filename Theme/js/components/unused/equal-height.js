App.equalHeight = (function($) {
  "use strict";

  function equalHeight(data) {
      var defaults = {
        elements: false
      }
      var settings = $.extend(defaults, data);
      var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        currentDiv,
        topPosition = 0,
        currentDivPadding = 0;

        $(settings.elements).height("auto");

        $(settings.elements).each(function() {

         $el = $(this);

         topPosition = $el.position().top;

         if (currentRowStart != topPosition) {

           // we just came to a new row.  Set all the heights on the completed row
           for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {

             currentDivPadding = parseInt(rowDivs[currentDiv].css("padding-top"))+parseInt(rowDivs[currentDiv].css("padding-bottom"));
             rowDivs[currentDiv].height(currentTallest-currentDivPadding);

           }

           // set the variables for the new row
           rowDivs.length = 0; // empty the array
           currentRowStart = topPosition;
           currentTallest = $el.outerHeight();
           rowDivs.push($el);

         } else {

           // another div on the current row.  Add it to the list and check if it's taller
           rowDivs.push($el);
           currentTallest = (currentTallest < $el.outerHeight()) ? ($el.outerHeight()) : (currentTallest);

        }

        // do the last row
         for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {

             currentDivPadding = parseInt(rowDivs[currentDiv].css("padding-top"))+parseInt(rowDivs[currentDiv].css("padding-bottom"));
             rowDivs[currentDiv].height(currentTallest-currentDivPadding);
         }

       });​
    }
  return {
    equalHeight: equalHeight
  }
}(window.jQuery));
