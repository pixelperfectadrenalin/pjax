App.relatedarticles = (function($) {
  "use strict";
  var _Initialised = false;
  var _hasPlayed = false;

  function init() {
    if (!_Initialised && $(".relatedarticles-list").length) {
      _Initialised = true;
      initEvents();
    }
  }

  function initEvents() {


            equalHeight();

            $(window).on("debouncedresize", function() {
              App.relatedarticles.equalHeight();
            })

            $(window).on("load", function() {
              App.relatedarticles.equalHeight();
            })




  }


  function equalHeight(){

    App.equalHeight.equalHeight({elements:".relatedarticles-item-content"});

  }


  return {
    init: init,
    equalHeight: equalHeight
  }
}(window.jQuery));
