App.popupzoom = (function($) {
    "use strict";
    var _Initialised = false;


    function onMagnificOpen(callerElement){




                        var $mfp = $(".mfp-zoomin");

                        var mfpContent = $(".mfp-content").children()[0];


                        // Get the first position.
                        var first = callerElement.getBoundingClientRect();

                        $.magnificPopup.callerElementBounding = first;

                        // // Now set the element to the last position.
                        // el.classList.add('totes-at-the-end');

                          $mfp.addClass('mfp-visible');



                        // Read again. This forces a sync layout, so be careful.
                        var last = mfpContent.getBoundingClientRect();


                        // _Initialised++;
                        // if(_Initialised==2){
                        //   a();
                        // }

                        // You can do this for other computed styles as well, if needed.
                        // Just be sure to stick to compositor-only props like transform
                        // and opacity where possible.
                        var invertY = first.top - last.top;
                        var invertX = first.left - last.left;
                        var invertHeight = first.height / last.height;

                        var invertWidth = first.width / last.width;

                          $mfp.removeClass('mfp-visible');
                          $mfp.removeClass('mfp-animations-enabled');


                        // Invert.
                        var translateString = 'translateY(' + invertY + 'px) translateX(' + invertX + 'px) scale(' + invertWidth + ',' + invertHeight + ')';



                        // mfpContent.addEventListener('transitionend', tidyUpAnimations);
                        // mfpContent.addEventListener('webkitTransitionend', tidyUpAnimations);



                        requestAnimationFrame(function() {

                          mfpContent.style.transform = translateString;
                          mfpContent.style.webkitTransform = translateString;


                          // Wait for the next frame so we know all the style changes have taken hold.
                          requestAnimationFrame(function() {

                            // Switch on animations.
                            $mfp.addClass('mfp-visible');
                            $mfp.addClass('mfp-animations-enabled');

                            // GO GO GOOOOOO!
                            mfpContent.style.transform = '';
                            mfpContent.style.webkitTransform = '';
                          });



                        });




                        // Capture the end with transitionend



    }


    function tidyUpAnimations(){


                        var $mfp = $(".mfp-zoomin");
                          $mfp.removeClass('mfp-animations-enabled');
                        var mfpContent = $(".mfp-content").children()[0];

                          mfpContent.style.transform = '';
                          mfpContent.style.webkitTransform = '';



                        mfpContent.removeEventListener('transitionend',tidyUpAnimations);
                        mfpContent.removeEventListener('webkitTransitionend',tidyUpAnimations);




    }


//     function tidyUpAnimationsRemove(){

// console.log("cleaned")

//                         var $mfp = $(".mfp-zoomin");
//                           $mfp.removeClass('mfp-animations-enabled');
//                         var mfpContent = $(".mfp-content").children()[0];

//                           mfpContent.style.transform = '';
//                           mfpContent.style.webkitTransform = '';



//                         mfpContent.removeEventListener('transitionend',tidyUpAnimations);
//                         mfpContent.removeEventListener('webkitTransitionend',tidyUpAnimations);




//     }


    function onMagnificClose(callerElement){



                        var $mfp = $(".mfp-zoomin");

                        var mfpContent = $(".mfp-content").children()[0];



                        // Get the first position.
                        var last = callerElement.getBoundingClientRect();

                        if(!last.height){


                          last =  $.magnificPopup.callerElementBounding;


                        }


                        // // Now set the element to the last position.
                        // el.classList.add('totes-at-the-end');

                        // Read again. This forces a sync layout, so be careful.
                        var first = mfpContent.getBoundingClientRect();

                        // You can do this for other computed styles as well, if needed.
                        // Just be sure to stick to compositor-only props like transform
                        // and opacity where possible.
                        var invertY = last.top - first.top;
                        var invertX = last.left - first.left;
                        var invertHeight = last.height / first.height;

                        var invertWidth = last.width / first.width;

                          $mfp.addClass('mfp-animations-enabled-removing');

                        // Invert.
                        var translateString = 'translateY(' + invertY + 'px) translateX(' + invertX + 'px) scale(' + invertWidth + ',' + invertHeight + ')';




                        mfpContent.addEventListener('transitionend', tidyUpAnimations);
                        mfpContent.addEventListener('webkitTransitionend', tidyUpAnimations);




                        // // Wait for the next frame so we know all the style changes have taken hold.
                        requestAnimationFrame(function() {

                        //   // Switch on animations.
                        //   // $mfp.addClass('mfp-animations-enabled');

                        //   // GO GO GOOOOOO!
                        mfpContent.style.transform = translateString;
                        mfpContent.style.webkitTransform = translateString;



                          setTimeout(function() {
                            mfpContent.style.transform = '';
                            mfpContent.style.webkitTransform = '';
                          },400)


                        // mfpContent.style.opacity = "0";

                        });

                        // Capture the end with transitionend
                        // mfpContent.addEventListener('transitionend', tidyUpAnimations);





    }



    return {
        // init: init,
        // openPopup: openPopup,
        onMagnificOpen: onMagnificOpen,
        onMagnificClose: onMagnificClose

    }
}(window.jQuery));
