App.login = (function($) {
  "use strict";
  var _Initialised = false;
  var _hasPlayed = false;

  function init() {
    if (!_Initialised && $(".herologin").length) {
      _Initialised = true;
      initEvents();
    }
  }

  function initEvents() {
    // if ($(".herologinbackground-video-box").length && !Modernizr.touch) {
    //   initVideo();
    // }
    if ($(".herologinbackground-image-box").length) {
      initImages();
    }




    // $(".js-forgotpassword").on("click",function(){

    //   showForgot();

    // });

    // $(".js-backtologin").on("click",function(){

    //   showLogin();

    // });

    // $(".js-forgotpasswordform").on("submit",function(e){

    //   e.preventDefault();

    //   if($(".js-forgotpasswordform").valid()){

    //     showForgotThanks();

    //   }

    // });




  }





  function showLogin(){

    $(".loginform:not(.loginform--login)").hide(0, function(){
      $(".loginform--login").show(0);
    });


  }

  function showForgot(){

    $(".loginform:not(.loginform--forgot)").hide(0, function(){
      $(".loginform--forgot").show(0);
    });

  }
  function showForgotThanks(){


    $(".loginform:not(.loginform--forgotthanks)").hide(0, function(){
      $(".loginform--forgotthanks").show(0);
    });

  }


  function initImages() {
      App.login.handleResize();
      $(".herologinbackground-image-mobile, .herologinbackground-image-desktop").unveil(0, function() {
        finishUnveilingMobileOrDesktop();
      });
    $(window).on("debouncedresize", function() {
      App.login.handleResize();
    })
  }


  function finishUnveilingMobileOrDesktop() {
    $(".herologinbackground").waitForImages(function() {
      finishedLoadingBackground();
    });
    // handleResize();
  }

  function finishedLoadingBackground() {
    $(".herologinbackground-inner").css("visibility", "visible");
    $(".herologinbackground-content").addClass("is-loaded");
  }



  function balanceText() {
    // App.balanceText.add($(".herologin-content-title"), {
    //   minimumLines: 1
    // });
    // App.balanceText.add($(".herologin-content-subtitle"), {
    //   minimumLines: 1
    // });
  }

  function handleResize() {

    var windowHeight = viewport().height;
    var windowWidth = viewport().width;

    var headerHeight, imageHeight, utilityHeight, contentTopMinumum, contentBottomMinumum;



      headerHeight = 155;

      imageHeight = 817;

      utilityHeight = 0;


      contentTopMinumum = 0;//200;

      contentBottomMinumum = 0;




    $(".herologin").height("auto");
    var contentMinHeight = $(".herologin").height() + contentTopMinumum + contentBottomMinumum;



    if (windowHeight > imageHeight) {

      windowHeight = imageHeight;

    }






      var otherThings =  headerHeight + utilityHeight;




      var contentHeight = windowHeight - otherThings;




      if (contentHeight < contentMinHeight) {
        contentHeight = contentMinHeight;
      }





      $(".herologin").height(contentHeight);


      $(".herologinbackground-image-box").height(contentHeight + otherThings);
      $(".herologinbackground-inner").height(contentHeight + otherThings);



  }
  return {
    init: init,
    handleResize: handleResize,
    showLogin: showLogin,
    showForgot: showForgot,
    showForgotThanks: showForgotThanks
  }
}(window.jQuery));
