App.dashpaymentdetails = (function($) {
    "use strict";
    var _Initialised = false;
    var didValidate = false;

    var oldSize;

    var $animationDiv;

    var numberOfLinkedAccounts = 0;



    function init() {
        if (!_Initialised && $(".dashpaymentdetails").length) {
            _Initialised = true;
            initEvents();

            equalHeight();

            $(window).on("debouncedresize", function() {
              App.dashpaymentdetails.equalHeight();
            })

            $(window).on("load", function() {
              App.dashpaymentdetails.equalHeight();
            })


        }
    }




    function initEvents() {


      $(".popup-editform-linkedaccounts-addanother").on("click",function(){

        var $numberOfLinkedAccountsField = $(".popup-editform--paymentdetails input[name=numberoflinkedaccounts]")

        var numberOfLinkedAccounts = $numberOfLinkedAccountsField.val();

        numberOfLinkedAccounts++;

        $numberOfLinkedAccountsField.val(numberOfLinkedAccounts);


        updateLinkedAccounts(numberOfLinkedAccounts);


      })



function updateLinkedAccounts(numberOfLinkedAccounts){


        var animationDuration = $.magnificPopup.instance.isOpen?400:0;


    for (var i = 0; i < 3; i++) {

        if(i<numberOfLinkedAccounts){

            $(".popup-editform-linkedaccounts-item").eq(i).slideDown(animationDuration).addClass("is-visible");
        }else{
            $(".popup-editform-linkedaccounts-item").eq(i).hide().removeClass("is-visible");
        }
    };


        if(numberOfLinkedAccounts==3){
          $(".popup-editform-linkedaccounts-addanother").slideUp(animationDuration);
        }


}





      // $(".popup-editform-cancel-link").on("click",function(){

      //   $.magnificPopup.close();

      // })

        $(".dashpaymentdetails-payments-item-tools-item.is-delete").on("click",function(){

            var $theItem  = $(this).parents(".dashpaymentdetails-payments-item");

            var theID = $theItem.find("input[name=id]").val();

            if(confirm("Do you really want to delete the payment?")){
                $("#dashpaymentdetailsdeleteid").val(theID)
                $(".dashpaymentdetails-form--delete")[0].submit();
            }




        })


        $(".dashpaymentdetails-payments-item-addanother").on("click",function(){


            var paymentType;

            if($(this).hasClass("is-creditcard")){
                paymentType = "creditcard"
            }else if($(this).hasClass("is-bank")){
                paymentType = "bank"
            }

            updateLinkedAccounts(1)

            App.form.resetForm($(".popup-editform--paymentdetails"));


            openPopup("add",paymentType,this)

        })

        $(".dashpaymentdetails-payments-item-tools-item.is-edit").on("click",function(){


          var $item = $(this).parents(".dashpaymentdetails-payments-item");


            App.form.resetForm($(".popup-editform--paymentdetails"));

            App.form.copyValuesFromFormToForm($item, $(".popup-editform--paymentdetails"))

            var numberOfLinkedAccounts = $(".popup-editform--paymentdetails input[name=numberoflinkedaccounts]").val();

            $(".popup-editform-linkedaccounts-addanother").show();

            updateLinkedAccounts(numberOfLinkedAccounts)

            var paymentType = $(".popup-editform--paymentdetails input[name=paymenttype]").val();


            openPopup("edit", paymentType,$item[0]);


        })

    }







  function equalHeight(){

    App.equalHeight.equalHeight({elements:".dashpaymentdetails-payments-item"});

  }


  function openPopup(addOrEdit,paymentType,callerElement){




        var titleVerb;
        var buttonCaption;
        if(addOrEdit=="add"){
            titleVerb = "Add";
            buttonCaption = "Save Details"
        }else if(addOrEdit=="edit"){
            titleVerb = "Edit";
            buttonCaption = "Update Details"
        }

        var titlePaymentType;
        if(paymentType=="creditcard"){
            titlePaymentType = "Credit Card";
        }else if(paymentType=="bank"){
            titlePaymentType = "Bank Account";
        }

            $(".popup-editform--paymentdetails .button-popupsubmit").text(buttonCaption)


        $(".popup-editform-intro-title").text(titleVerb+" payment details - Your "+titlePaymentType);

        $(".popup-editform-creditcardordirectdebit-creditcard").toggle(paymentType=="creditcard");
        $(".popup-editform-creditcardordirectdebit-directdebit").toggle(paymentType=="bank");

        // $.magnificPopup.open({

        //     // prependTo: $("form:first"),
        //     // delegate: 'a',
        //     items: {
        //         src: '.popup-editform--paymentdetails',
        //         type: 'inline'
        //     },
        //     // tLoading: 'Loading image #%curr%...',
        //     mainClass: 'mfp-fade popup-editform-outer',


        //     closeBtnInside: true,
        //     preloader: false,
        //     removalDelay: 150, // Delay to allow for fade out
        // });



        App.popupeditform.openPopup('.popup-editform--paymentdetails',callerElement)


  }




    return {
        init: init,
        equalHeight: equalHeight
    }
}(window.jQuery));
