App.fonts = (function($) {
    "use strict";
    var _Initialised = false;

    var customFonts = ["Saturday Sans Regular", "Saturday Sans Bold"];//, "Helvetica Neue"];

    var loadCount = 0;

    var hadError = false;


    function init() {
        if (!_Initialised) {
            _Initialised = true;
            initEvents();
        }
    }

    function initEvents() {


      for (var i = 0; i < customFonts.length; i++) {

        var observer = new FontFaceObserver(customFonts[i],{});

        observer.check(null, 20000).then(onFontLoad,onFontError);


      };




    }

    function onFontLoad(){
      proceed()


    }


    function onFontError(){
      hadError = true;

      console.log("font load error.");
      proceed()

    }



    function proceed(){

      loadCount++;
      if(loadCount==customFonts.length){


        App.onFontsLoad();

        // if(!hadError){
            document.documentElement.className += " wf-active";
        // }

      }

    }

    return {
        init: init
    }
}(window.jQuery));
