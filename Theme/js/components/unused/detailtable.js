App.detailtable = (function($) {
  //"use strict";
  var _Initialised = false;

  var _InitialisedEvents = false;
  var _hasPlayed = false;

  var isPopupOpen = false;



  function init() {
    "use strict";
    if (!_Initialised && $(".detailtable").length) {
      _Initialised = true;
      initEvents();

      fixTableForIE($("body"));

    }
  }


  function fixTableForIE($element){


        if(detectIE()){


                  fixTableForIEProcess($element);

                  if(!_InitialisedEvents){

                    $(window).on("debouncedresize", function() {
                      fixTableForIEProcess();
                    })


                    $(window).on("load", function() {
                      fixTableForIEProcess();
                    })

                    _InitialisedEvents = true

                  }

            }


  }

  function fixTableForIEProcess($element){

    if(!$element){
      $element = $("body")
    }


    $element.find(".detailtable-documents-cta").height("auto");

    $element.find(".detailtable-documents-cta").each(function(){

      $(this).height($(this).parent().outerHeight());

    });

  }




  function initEvents() {


    $(".detailtable-documents-cta.is-submissions").on("click",function(e){

        e.preventDefault();


        openPopup($(this).attr("href"), this,function(){

          App.detailtable.fixTableForIE($(".mfp-content"))

        });


    });





    $("body").on("click",".detailreports-pagination a", function(e){

      e.preventDefault();

      openPopupFromInternalLink($(this).attr("href"))

    });


  }



function fixTable(){

  // console.log("aaa")

          App.detailtable.fixTableForIE($(".mfp-content"))

      }



function openPopupFromInternalLink(URL){


        $.magnificPopup.open({


            items: {
                src: URL,
                type: "ajax"
            }

          });


        }



    function openPopup(element, callerElement, onOpen, onClose){


        var popupType;

        var popupCallback;


              if(typeof element==="string"){

                popupType = "ajax";

              }else{

                popupType = "inline";

              }



                $.magnificPopup.open({

                    // prependTo: $("form:first"),
                    // delegate: 'a',
                    items: {
                        src: element,
                        type: popupType
                    },
                    // tLoading: 'Loading image #%curr%...',
                    mainClass: 'popup-submissions-outer mfp-zoomin',


                    closeBtnInside: true,
                    preloader: false,
                    removalDelay: 400, // Delay to allow for fade out

                    preloader: true,

                    callbacks: {


                      buildControls: function() {
                        // re-appends controls inside the main container
                        // this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                      }

                      ,

                      ajaxContentAdded: function() {


                        if(popupType == "ajax"){

                          if(typeof onOpen == "function"){
                              onOpen()
                            }

                          if(!isPopupOpen){

                            isPopupOpen = true;


                            App.popupzoom.onMagnificOpen(callerElement);

                            }

                        }



                      },
                      open: function() {


                        if(popupType == "inline"){

                          if(typeof onOpen == "function"){
                              onOpen()
                            }

                          if(!isPopupOpen){

                            isPopupOpen = true;


                            App.popupzoom.onMagnificOpen(callerElement);

                            }

                        }





                      },

                      close: function() {
                        isPopupOpen = false;

                        // Will fire when popup is closed
                        if(typeof onClose == "function"){
                          onClose()
                        }

                        // $(".js-popup-dash-close").off("click.popupdash");

                      },
                      beforeClose:function(){

                        App.popupzoom.onMagnificClose(callerElement);

                      },

                      // close: function() {
                      //   // console.log('Popup removal initiated (after removalDelay timer finished)');
                      // },
                      // afterClose: function() {
                      //   // console.log('Popup is completely closed');
                      // }
                    }

                });

}






//http://codepen.io/gapcode/pen/vEJNZN

function detectIE() {
  var ua = window.navigator.userAgent;

  // Test values; Uncomment to check result …

  // IE 10
  // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

  // IE 11
  // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

  // IE 12 / Spartan
  // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

  // Edge (IE 12+)
  // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    var rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  // other browser
  return false;
}




  return {
    init: init,
    fixTableForIE: fixTableForIE
  }
}(window.jQuery));
