/*****************************
SuperLink by Stephen Rose 2014
         version 0.1
******************************/

;
(function($, window, document, undefined) {
    var pluginName = "superlink",
        defaults = {
        };

    function Plugin(element, options) {

        this.element = element;
        this.$element = $(element);
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();

    }
    $.extend(Plugin.prototype, {
        init: function() {
            this._initEvents();
        },

        _initEvents: function() {

            $(document).on("mouseup touchend",function() {
                if (window.superLinkMouseDown) {
                    $(window.superLinkActiveElement).removeClass("is-superlink-active");
                    $(window.superLinkActiveElement).find(".superlink-target").removeClass("is-superlink-active");
                    window.superLinkMouseDown = false;
                }
            });

            $(".superlink-target").on("click",function(e){
                e.stopPropagation();
            });

            this.$element.on("click",function(e){
                e.stopPropagation();
                console.log("aaa")
                $(this).find(".superlink-target")[0].click();

            });

            this.$element.on("mousedown touchstart",function(){
                window.superLinkMouseDown = true;
                window.superLinkActiveElement = $(this);
                $(this).addClass("is-superlink-active");
                $(this).find(".superlink-target").addClass("is-superlink-active");
            });

            this.$element.on("mouseenter",function(){
                $(this).addClass("is-superlink-hovered");
                $(this).find(".superlink-target").addClass("is-superlink-hovered");
            });
            this.$element.on("mouseleave",function(){
                $(this).removeClass("is-superlink-hovered");
                $(this).find(".superlink-target").removeClass("is-superlink-hovered");
            });
            


        }
    });
    // You don't need to change something below:
    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations and allowing any
    // public function (ie. a function whose name doesn't start
    // with an underscore) to be called via the jQuery plugin,
    // e.g. $(element).defaultPluginName('functionName', arg1, arg2)
    $.fn[pluginName] = function(options) {


        var args = arguments;
        // Is the first parameter an object (options), or was omitted,
        // instantiate a new instance of the plugin.
        if (options === undefined || typeof options === 'object') {

            return $(".superlink").each(function() {
                // Only allow the plugin to be instantiated once,
                // so we check that the element has no plugin instantiation yet
                if (!$.data(this, 'plugin_' + pluginName)) {
                    // if it has no instance, create a new one,
                    // pass options to our plugin constructor,
                    // and store the plugin instance
                    // in the elements jQuery data object.
                    $.data(this, 'plugin_' + pluginName, new Plugin(
                        this, options));
                }
            });
            // If the first parameter is a string and it doesn't start
            // with an underscore or "contains" the `init`-function,
            // treat this as a call to a public method.
        } else if (typeof options === 'string' && options[0] !== '_' &&
            options !== 'init') {
            // Cache the method call
            // to make it possible
            // to return a value
            var returns;
            $(".superlink").each(function() {
                var instance = $.data(this, 'plugin_' +
                    pluginName);
                // Tests that there's already a plugin-instance
                // and checks that the requested public method exists
                if (instance instanceof Plugin && typeof instance[
                    options] === 'function') {
                    // Call the method of our plugin instance,
                    // and pass it the supplied arguments.
                    returns = instance[options].apply(instance,
                        Array.prototype.slice.call(args, 1)
                    );
                }
                // Allow instances to be destroyed via the 'destroy' method
                if (options === 'destroy') {
                    $.data(this, 'plugin_' + pluginName, null);
                }
            });
            // If the earlier cached method
            // gives a value back return the value,
            // otherwise return this to preserve chainability.
            return returns !== undefined ? returns : this;
        }
    };
})(jQuery, window, document);