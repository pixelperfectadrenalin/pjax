;
(function($, window, document, undefined) {
    var pluginName = "progressCircle",
        defaults = {
            strokeWidth: 10,
            width: 100,
            strokeColor: "black",
            fillColor: "transparent"
        };

    function Plugin(element, options) {

        this.element = element;
        this.$element = $(element);
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();

    }
    $.extend(Plugin.prototype, {
        init: function() {
            this._injectCircle();
        },

        _injectCircle: function() {

            var theWidth = this.settings.width;
            var theStrokeWidth = this.settings.strokeWidth;

            var theRadius = (theWidth / 2) - (theStrokeWidth / 2);

            var theCX = theWidth / 2;
            var theCY = theCX;

            var fillColor = this.settings.fillColor;
            var strokeColor = this.settings.strokeColor;

            var theCircumference = theRadius * Math.PI * 2;

            this.circumference = theCircumference;

            var theCircleSVG = '<svg class="progresscircle-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 ' + theWidth + ' ' + theWidth + '"><circle r="' + theRadius + '" cx="' + theCX + '" cy="' + theCX + '" fill="' + fillColor + '" stroke-width="' + theStrokeWidth + 'px" stroke="' + strokeColor + '" stroke-dasharray="' + theCircumference + '" style="stroke-dashoffset:' + theCircumference + ';"></circle></svg>';

            var theLabel = '<div class="progresscircle-label"></div>';


            this.$element.append(theCircleSVG);
            this.$element.append(theLabel);


        },

        setPercent: function(percent) {

            if (percent < 0) {
                percent = 0;
            }
            if (percent > 100) {
                percent = 100;
            }

            var theStrokeDashoffset = ((100 - percent) / 100) * this.circumference;


            this.$element.find("circle").css({
                strokeDashoffset: theStrokeDashoffset
            });
            this.$element.attr("data-percent",parseInt(percent));

        }



    });
    // You don't need to change something below:
    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations and allowing any
    // public function (ie. a function whose name doesn't start
    // with an underscore) to be called via the jQuery plugin,
    // e.g. $(element).defaultPluginName('functionName', arg1, arg2)
    $.fn[pluginName] = function(options) {


        var args = arguments;
        // Is the first parameter an object (options), or was omitted,
        // instantiate a new instance of the plugin.
        if (options === undefined || typeof options === 'object') {

            return this.each(function() {
                // Only allow the plugin to be instantiated once,
                // so we check that the element has no plugin instantiation yet
                if (!$.data(this, 'plugin_' + pluginName)) {
                    // if it has no instance, create a new one,
                    // pass options to our plugin constructor,
                    // and store the plugin instance
                    // in the elements jQuery data object.
                    $.data(this, 'plugin_' + pluginName, new Plugin(
                        this, options));
                }
            });
            // If the first parameter is a string and it doesn't start
            // with an underscore or "contains" the `init`-function,
            // treat this as a call to a public method.
        } else if (typeof options === 'string' && options[0] !== '_' &&
            options !== 'init') {
            // Cache the method call
            // to make it possible
            // to return a value
            var returns;
            this.each(function() {
                var instance = $.data(this, 'plugin_' +
                    pluginName);
                // Tests that there's already a plugin-instance
                // and checks that the requested public method exists
                if (instance instanceof Plugin && typeof instance[
                    options] === 'function') {
                    // Call the method of our plugin instance,
                    // and pass it the supplied arguments.
                    returns = instance[options].apply(instance,
                        Array.prototype.slice.call(args, 1)
                    );
                }
                // Allow instances to be destroyed via the 'destroy' method
                if (options === 'destroy') {
                    $.data(this, 'plugin_' + pluginName, null);
                }
            });
            // If the earlier cached method
            // gives a value back return the value,
            // otherwise return this to preserve chainability.
            return returns !== undefined ? returns : this;
        }
    };
})(jQuery, window, document);