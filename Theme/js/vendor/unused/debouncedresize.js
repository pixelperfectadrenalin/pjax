/*
 * debouncedresize: special jQuery event that happens once after a window resize
 *
 * latest version and complete README available on Github:
 * https://github.com/louisremi/jquery-smartresize
 *
 * Copyright 2012 @louis_remi
 * Licensed under the MIT license.
 *
 * This saved you an hour of work? 
 * Send me music http://www.amazon.co.uk/wishlist/HNTU0468LQON
 */


(function($) {


    //Includes a test to see if window actually changed width, as IE8 will sometimes randomly fire resize if elements are modified.
    //Also if touch (mobile) device ignore height as toolbar hiding/showing can cause height to change


var $event = $.event,
    $special = {},
    resizeTimeout,
    winOldWidth, 
    winNewWidth,
    winOldHeight,
    winNewHeight;



    //New height and width

    winOldWidth = $(window).width();
    winOldHeight = $(window).height();

    if(Modernizr.touch){

        //If touch (mobile) device ignore height as toolbar hiding/showing can cause height to change

        $special.winNewHeight = 0;

    }


$special = $event.special.debouncedresize = {
    setup: function() {


        $( this ).on( "resize", $special.handler );
    },
    teardown: function() {
        $( this ).off( "resize", $special.handler );
    },
    handler: function( event, execAsap ) {
        // Save the context
        var context = this,
            args = arguments,
            dispatch = function() {
                // set correct event type
                event.type = "debouncedresize";
                $event.dispatch.apply( context, args );

                winOldWidth = winNewWidth;
                winOldHeight = winNewHeight;

                if(Modernizr.touch){

                    //If touch (mobile) device ignore height as toolbar hiding/showing can cause height to change

                    winOldHeight = 0;

                }


            };

        if ( resizeTimeout ) {
            clearTimeout( resizeTimeout );
        }

        //New height and width
        winNewWidth = $(window).width();
        winNewHeight = $(window).height();

        if(Modernizr.touch){

            //If touch (mobile) device ignore height as toolbar hiding/showing can cause height to change

            winNewHeight = 0;

        }



        // compare the new height and width with old one
        if(winOldWidth!=winNewWidth || winOldHeight!=winNewHeight){


            execAsap ?
                dispatch() :
                resizeTimeout = setTimeout( dispatch, $special.threshold );



        }




    },
    threshold: 10
};

})(jQuery);

