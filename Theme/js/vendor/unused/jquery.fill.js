/*
 * Plugin Name: Resize Image to Parent Container
 *
 * Author: Christian Varga
 * Author URI: http://christianvarga.com
 * Plugin Source: https://github.com/levymetal/jquery-resize-image-to-parent/
 *
 */

(function($) {
  $.fn.resizeToParent = function(opts) {
    var defaults = {
     parent: false,
     delay: 100,
     horizontalAlign:"center",
     verticalAlign:"middle",
     force:"none"
    }

    var opts = $.extend(defaults, opts);

    function positionImage(obj) {
      // reset image (in case we're calling this a second time, for example on resize)
      obj.css({'width': '', 'height': '', 'margin-left': '', 'margin-top': ''});

      // dimensions of the parent

      var theParent =opts.parent ? obj.parents(opts.parent):obj.parent();

      var parentWidth = theParent.outerWidth();
      var parentHeight = theParent.outerHeight();


      // // dimensions of the image
      // var imageWidth = obj.width();
      // var imageHeight = obj.height();

      // dimensions of the image - override with data attr if we have them



      var imageWidth = obj.data("width") ? obj.data("width") : obj.width();
      var imageHeight = obj.data("height") ? obj.data("height") : obj.height();

      // step 1 - calculate the percentage difference between image width and container width
      var diff = imageWidth / parentWidth;
    var resizeBy;

      // step 2 - if height divided by difference is smaller than container height, resize by height. otherwise resize by width
      if (opts.force == "height"|| (imageHeight / diff) < parentHeight) {
        resizeBy = "height";

       // set image variables to new dimensions
       imageWidth = imageWidth / (imageHeight / parentHeight);
       imageHeight = parentHeight;
       obj.css({'width': imageWidth, 'height': parentHeight});
      }
      else {
        resizeBy = "width";

       // set image variables to new dimensions
       imageWidth = parentWidth;
       imageHeight = imageHeight / diff;
       obj.css({'height': imageHeight, 'width': parentWidth});
      }


      // step 3 - center image in container
      if(opts.horizontalAlign=="left"){
        
        var leftOffset = 0;

      }else if(opts.horizontalAlign=="center"){

        var leftOffset = (imageWidth - parentWidth) / -2;//center

      }else if(opts.horizontalAlign=="right"){
        
        var leftOffset = (imageWidth - parentWidth)*-1;//right

      }








      if(opts.verticalAlign=="top"){
        
      var topOffset = 0;

      }else if(opts.verticalAlign=="middle"){

      var topOffset = (imageHeight - parentHeight) / -2;

      }else if(opts.verticalAlign=="bottom"){
        
        var topOffset = (imageHeight - parentHeight) * -1;

      }




      obj.css({'margin-left': leftOffset, 'margin-top': topOffset,"visibility":"visible"});

      

    }

    // run the position function on window resize (to make it responsive)
    var tid;
    var elems = this;

    $(window).on("debouncedresize", function() {

      clearTimeout(tid);
      tid = setTimeout(function() {
        elems.each(function() {
          positionImage($(this));
        });
      }, opts.delay);
    });


    return this.each(function() {
      var obj = $(this);

      // hack to force ie to run the load function... ridiculous bug 
      // http://stackoverflow.com/questions/7137737/ie9-problems-with-jquery-load-event-not-firing
      obj.attr("src", obj.attr("src"));



      //if we have data attr, don't wait till loaded, run straight away
      if(obj.data("width")||obj.data("height")){

        positionImage(obj);

      }else{

      // bind to load of image
        obj.load(function() {
          positionImage(obj);
        });


      }

      // run the position function if the image is cached
      if (this.complete) {
        positionImage(obj);
      }
    });
  }
})( jQuery );