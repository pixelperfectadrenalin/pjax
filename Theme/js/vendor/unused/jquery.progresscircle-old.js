// jQuery Plugin Boilerplate
// A boilerplate for jumpstarting jQuery plugins development
// version 1.1, May 14th, 2011
// by Stefan Gabos

// remember to change every instance of "progressCircle" to the name of your plugin!
(function($) {

    // here we go!
    $.progressCircle = function(element, options) {

        // plugin's default options
        // this is private property and is  accessible only from inside the plugin





		var defaults = {
		strokeWidth: 10,
		width: 100,
		strokeColor: "black",
		fillColor: "white"
		};


        // to avoid confusions, use "plugin" to reference the 
        // current instance of the object
        var plugin = this;

        // this will hold the merged default, and user-provided options
        // plugin's properties will be available through this object like:
        // plugin.settings.propertyName from inside the plugin or
        // element.data('progressCircle').settings.propertyName from outside the plugin, 
        // where "element" is the element the plugin is attached to;
        plugin.settings = {}

        var $element = $(element), // reference to the jQuery version of DOM element
             element = element;    // reference to the actual DOM element

        // the "constructor" method that gets called when the object is created
        plugin.init = function() {

            // the plugin's final properties are the merged default and 
            // user-provided options (if any)
            plugin.settings = $.extend({}, defaults, options);

            // code goes here

						// plugin.$element = $(plugin.element);

						injectCircle();
        }




				var injectCircle = function() {
					// console.log($element)

					var theWidth = plugin.settings.width;
					var theStrokeWidth = plugin.settings.strokeWidth;
					
					var theRadius = (theWidth/2)-(theStrokeWidth/2);

					var theCX = theWidth/2;
					var theCY = theCX;

					var fillColor = plugin.settings.fillColor;
					var strokeColor = plugin.settings.strokeColor;

					var theCircumference = theRadius * Math.PI * 2;

					plugin.circumference = theCircumference;

					var theCircleSVG = '<svg class="progresscircle" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 '+theWidth+' '+theWidth+'"><circle id="bar" r="'+theRadius+'" cx="'+theCX+'" cy="'+theCX+'" fill="'+fillColor+'" stroke-width="'+theStrokeWidth+'px" stroke="'+strokeColor+'" stroke-dasharray="'+theCircumference+'" style="stroke-dashoffset:'+theCircumference+';"></circle></svg>';


					$element.append(theCircleSVG);


				}

				plugin.setPercent = function(percent){

				    if (percent < 0) { percent = 0;}
				    if (percent > 100) { percent = 100;}
				    
				    var theStrokeDashoffset = ((100-percent)/100)*plugin.circumference;

				    console.log(theStrokeDashoffset)

				    $element.find("circle").css({ strokeDashoffset: theStrokeDashoffset});

				}


        // fire up the plugin!
        // call the "constructor" method
        plugin.init();

    }

    // add the plugin to the jQuery.fn object
    $.fn.progressCircle = function(options) {

        // iterate through the DOM elements we are attaching the plugin to
        return this.each(function() {

            // if plugin has not already been attached to the element
            if (undefined == $(this).data('progressCircle')) {

                // create a new instance of the plugin
                // pass the DOM element and the user-provided options as arguments
                var plugin = new $.progressCircle(this, options);

                // in the jQuery version of the element
                // store a reference to the plugin object
                // you can later access the plugin and its methods and properties like
                // element.data('progressCircle').publicMethod(arg1, arg2, ... argn) or
                // element.data('progressCircle').settings.propertyName
                $(this).data('progressCircle', plugin);

            }

        });

    }

})(jQuery);