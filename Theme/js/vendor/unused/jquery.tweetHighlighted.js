/*
 * TweetHighlighted - jQuery plugin that brings a 'tweet' button
 *                    when a text is selected.
 *
 */
 
(function($) {
    $.fn.tweetHighlighted = function(options) {
        var settings = {},
            classes;
        options = options || {};

        var $button;

        var lastSelectedText = "";

        var isOpen = false;

        // html DOM element to use as a button
        settings.node = options.node || '<button type="button">Tweet</button>';
        // css class to add to the html node
        settings.cssClass = options.cssClass || 'tweet-me';
        // minimum length of the selected text for the tweet button to appear
        settings.minLength = options.minLength || 1;
        // maximum length of the selected text for the tweet button to appear
        settings.maxLength = options.maxLength || 140;
        // extra content to attach (mostly used to add URLs)
        settings.extra = options.extra || '';
        // twitter 'via' handle
        // (basically appends 'via @twitterhandle' to the tweet)
        settings.via = options.via || null;
        // arguments to pass to the window.open() function

        settings.popupWidth = 550;
        settings.popupHeight = 420;

        var popupLeft = (window.screen.availWidth - settings.popupWidth) / 2;
        var popupTop = (window.screen.availHeight - settings.popupHeight) / 2;

        settings.popupArgs = options.popArgs || "status=1,width=" + settings.popupWidth + ",height=" + settings.popupHeight + ",top=" + popupTop + ",left=" + popupLeft;




        // get an array of classes filtering out empty whitespaces
        classes = settings.cssClass.split(' ').filter(function(item) {
            return item.length;
        });
        settings._selector = '.' + classes.join('.');

        // event that fires when any non-empty text is selected
        var onTextSelect = function(selector, callback) {
            function getSelectedText() {
                var selection = "";

                if(document.activeElement.tagName=="INPUT"||document.activeElement.tagName=="TEXTAREA"){

                    return "";
                    
                }

                if (window.getSelection) {
                    selection = window.getSelection().toString();
                } else if (document.selection) {
                    selection = document.selection.createRange().text;
                }
                selection = selection.replace(/(\r\n|\n|\r)/gm," ").replace(/\s+/g,' ').replace(/(^\s+|\s+$)/g,'');

                selection = truncate(selection,139);
                
                return selection;


            };

            // fires the callback when text is selected

            $(selector).on("mouseup", function(e) {

                var text = getSelectedText();


                if (text != lastSelectedText && text !== "") {
                    callback(e, text);
                }

            });

            // removes the button when the selected text loses focus
            $(document).click(function(e) {
                setTimeout(function(){
                    var text = getSelectedText();
                    if (text == "") {
                        lastSelectedText = "";
                        hideButton();
                    }
                },1)
            });
        };


        /**
         * Author: Andrew Hedges, andrew@hedges.name
         * License: free to use, alter, and redistribute without attribution
         */
         
        /**
         * Truncate a string to the given length, breaking at word boundaries and adding an elipsis
         * @param string str String to be truncated
         * @param integer limit Max length of the string
         * @return string
         */
        var truncate = function (str, limit) {
            var bits, i;
            if (typeof str != "string") {
                return '';
            }
            bits = str.split('');
            if (bits.length > limit) {
                for (i = bits.length - 1; i > -1; --i) {
                    if (i > limit) {
                        bits.length = i;
                    }
                    else if (' ' === bits[i]) {
                        bits.length = i;
                        break;
                    }
                }
                bits.push('…');
            }
            return bits.join('');
        };
        // END: truncate



        var hideButton = function(){

            if(isOpen){
                $button.fadeOut(500).remove();
                isOpen = false;
            }
        }

        var getTweetURL = function(text, extra, via) {
            var url = 'https://twitter.com/intent/tweet?text=';
            url += encodeURIComponent(text);

            if (extra)
                url += encodeURIComponent(' ' + extra);

            if (via)
                url += '&via=' + via;

            return url;
        };

        onTextSelect(this, function(e, text) {

            if (text.length > settings.maxLength || text.length < settings.minLength){
            
                return;

            }

            lastSelectedText = text;

            hideButton();


            var url = getTweetURL(text, settings.extra, settings.via);


            $button = $(settings.node).addClass(settings.cssClass).offset({
                top: e.pageY,
                left: e.pageX
            }).css({
                position: 'absolute',
                cursor: 'pointer',
                display:"none"
            });
            $button.appendTo('body');

            $button.fadeIn(500);

            isOpen = true;

            $button.click(function(e) {

                e.preventDefault();
                hideButton();
                window.open(url, '_blank', settings.popupArgs);
            });
        });
    };
})(jQuery);