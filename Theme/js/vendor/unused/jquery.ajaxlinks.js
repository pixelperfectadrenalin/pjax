/*****************************
AjaxLinks by Stephen Rose 2014
         version 0.2
******************************/

;
(function($, window, document, undefined) {
    var pluginName = "ajaxlinks",
        defaults = {};

    function Plugin(element, options) {

        this.element = element;
        this.$element = $(element);
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
        this.initialized = false;

    }
    $.extend(Plugin.prototype, {
        init: function() {
            this._initEvents();
        },


        _updateProgress: function(data) {

            // data.percent, data.stage

            var stageTotal = 2; //HTML + Images

            finalPercent = (data.percent / stageTotal) + (((data.stage - 1) / stageTotal * 100))

            console.log(data.percent, finalPercent)

            $(".site-progress").stop().show().animate({
                width: finalPercent + "%"
            }, data.duration, function() {

                if (typeof data.callback == "function") {
                    data.callback();
                }


            });



        },

        _initEvents: function() {

            var self = this;


            History.Adapter.bind(window, 'statechange', function() { // code });
                var State = History.getState(); // Note: We are using History.getState() instead of event.state
                // History.log('statechange:', State.data, );

                // State.title, 
                self.loadURL(State.url)


            })




            $("body").on("click", ".ajaxlinks a, a.ajaxlinks", function(e) {

                e.preventDefault();

                var $element = $(this);


                var href = $element.attr("href");

                self.loadURL(href);

            })

        },



           loadURL: function(href) {

            var self = this;



                var xhr = new XMLHttpRequest();
                xhr.open('GET', href, true);



                History.pushState(null, null, href);


                // Hack to pass bytes through unprocessed.
                // xhr.overrideMimeType('text/plain; charset=x-user-defined');

                self._updateProgress({
                    percent: 0,
                    stage: 1,
                    duration:0
                });


                if ('onprogress' in xhr) {


                    xhr.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total * 100;
                            //Do something with download progress

                            // $(".site-progress").css({width:(percentComplete*100)+"%"});
                            console.log("ajax progress")
                            self._updateProgress({
                                percent: percentComplete,
                                stage: 1
                            })

                        }
                    }, false);



                } else {

                    self._updateProgress({
                        percent: 50,
                        stage: 1,
                        duration: 1000
                    })


                }






                xhr.onreadystatechange = function(e) {

                    if (e.currentTarget.readyState == 4 && e.currentTarget.status == 200) {


                        // Exclude scripts to avoid IE 'Permission Denied' errors
                        var responseHTML = jQuery("<div>").append(jQuery.parseHTML(e.currentTarget.responseText));

                        $(".ajaxlinkscontainer").html(

                            responseHTML.find(".ajaxlinkscontainer-inner")

                        );

                        $(window).scrollTop(0);

                        $('.ajaxlinkscontainer-inner').waitForImages(function() {

                            //Complete
                            console.log("images loaded complete")
                            self._updateProgress({
                                percent: 100,
                                stage: 2,
                                duration: 200,
                                callback: function() {

                                    $(".site-progress").hide();

                                }
                            })

                            // $(".site-progress").stop().animate({width:"100%"},100);


                            if (typeof App.onReadyScripts == "function") {
                                App.onReadyScripts()
                            }
                            if (typeof App.onLoadScripts == "function") {
                                App.onLoadScripts()
                            }



                        }, function(loaded, count, success) {
                            console.log(loaded + ' of ' + count + ' images has ' + (success ? 'loaded' : 'failed to load') + '.');
                            // $(this).addClass('loaded');

                            var percentComplete = ((loaded) / count) * 100;


                            // $(".site-progress").stop().animate({width:+"%"},500);

                            self._updateProgress({
                                percent: percentComplete,
                                stage: 2,
                                duration: 500
                            })


                        })

                        document.title = responseHTML.find("title").text();

                    }
                };


                xhr.send();

            }


    });
    // You don't need to change something below:
    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations and allowing any
    // public function (ie. a function whose name doesn't start
    // with an underscore) to be called via the jQuery plugin,
    // e.g. $(element).defaultPluginName('functionName', arg1, arg2)
    $.fn[pluginName] = function(options) {


        var args = arguments;
        // Is the first parameter an object (options), or was omitted,
        // instantiate a new instance of the plugin.
        if (options === undefined || typeof options === 'object') {

            if (!this.initialized) {

                this.initialized = true;

                new Plugin(options);
            }

            // If the first parameter is a string and it doesn't start
            // with an underscore or "contains" the `init`-function,
            // treat this as a call to a public method.
        } else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {
            // Cache the method call
            // to make it possible
            // to return a value
            var returns;

            if (!this.initialized) {

                this.initialized = true;

                new Plugin(options)

                if (instance instanceof Plugin && typeof instance[options] === 'function') {
                    // Call the method of our plugin instance,
                    // and pass it the supplied arguments.
                    returns = instance[options].apply(instance,
                        Array.prototype.slice.call(args, 1)
                    );

                }

            }

            // If the earlier cached method
            // gives a value back return the value,
            // otherwise return this to preserve chainability.
            return returns !== undefined ? returns : this;
        }
    };
})(jQuery, window, document);



/**
 * redirect.js
 *
 * Retrieve the url hidden behind and HTML4 history hash glitch
 *
 * @author Ludovic Decampy <ludovic.decampy@gmail.com>
 * @copyright 2014 Ludovic Decampy <ludovic.decampy@gmail.com>
 * @license MIT License <http://creativecommons.org/licenses/MIT/>
 */

(function() {
    if (window.location.hash !== '') {
        var a = window.location.href;
        //Replace root path 
        var b = a.replace(/^(http:\/\/[^\/]*\/)[^\#]*\#\/(.*)$/, '$1$2');
        //Replace relative path 
        b = b.replace(/^(http:\/\/[^\/]*\/[^\#]*\/)[^\#]*\#(\.\/)?(.*)$/, '$1$3');
        if (b !== a) {
            // Do whatever during the redirection ...
            window.location = b;
        }
    }
})();






(function($, window, undefined) {
    //is onprogress supported by browser?
    var hasOnProgress = ("onprogress" in $.ajaxSettings.xhr());

    //If not supported, do nothing
    if (!hasOnProgress) {
        return;
    }

    //patch ajax settings to call a progress callback
    var oldXHR = $.ajaxSettings.xhr;
    $.ajaxSettings.xhr = function() {
        var xhr = oldXHR();
        if (xhr instanceof window.XMLHttpRequest) {
            xhr.addEventListener('progress', this.progress, false);
        }

        if (xhr.upload) {
            xhr.upload.addEventListener('progress', this.progress, false);
        }

        return xhr;
    };
})(jQuery, window);