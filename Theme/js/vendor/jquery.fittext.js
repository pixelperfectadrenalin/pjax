// window.performance = window.performance || {};
// performance.now = (function() {
//     return performance.now       ||
//         performance.mozNow    ||
//         performance.msNow     ||
//         performance.oNow      ||
//         performance.webkitNow ||            
//         Date.now  /*none found - fallback to browser default */
// })();




;(function($) {
    $.fn.textfill = function(options) {
        var minFontPixels = options.minFontPixels;
        var maxFontPixels = options.maxFontPixels;
        fontSize = maxFontPixels;
        // var $ourText = $(this);
        var ourText = $(this)[0];

        ourText.style.fontSize = '1px';//Reduce text size to get correct offsetHeight

        var maxHeight = ourText.offsetHeight;
        var maxWidth = ourText.offsetWidth;
        var textHeight;
        var textWidth;




// var timeStart = performance.now();



        // do {
        //     ourText.style.fontSize = fontSize;
        //     textHeight = ourText.offsetHeight;
        //     textWidth = ourText.offsetWidth;
        //     fontSize = fontSize - 1;
        // } while ((textHeight > maxHeight || textWidth > maxWidth) && fontSize > minFontPixels);



        // binary search, actually slower??

        low = options.minFontPixels + 1;
        high = options.maxFontPixels + 1;

      // Binary search for best fit
      while ( low <= high) {
        mid = parseInt((low + high) / 2, 10);


        ourText.style.fontSize = mid + 'px';

        if(ourText.offsetWidth <= maxWidth && ourText.offsetHeight <= maxHeight){
          low = mid + 1;
        } else {
          high = mid - 1;
        }
      }




      // Sub 1 at the very end, this is closer to what we wanted.
      ourText.style.fontSize = (mid - 1) + 'px';





// var timeEnd = performance.now();


// console.log(timeEnd-timeStart)



        if(typeof options.callback=="function"){

            options.callback();

        }



        return this;
    }
})(jQuery);
