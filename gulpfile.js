var gulp = require('gulp');
var plumber = require('gulp-plumber');
var cssmin = require('gulp-cssmin');
var combineMq = require('gulp-combine-mq');
var bless = require('gulp-bless');
var autoprefixer = require('gulp-autoprefixer');
var connect = require('gulp-connect');
var kit = require('gulp-kit');
var sass = require('gulp-sass');
var less = require('gulp-less');
var uglify = require('gulp-uglify');
var less = require('gulp-less');
var parker = require('gulp-parker');
var concat = require('gulp-concat');
var runSequence = require('run-sequence');
var connect = require('gulp-connect');


gulp.task('connect', function() {
  connect.server({
    port: 2222,
    root: [__dirname],
    livereload: true
  });
});



gulp.task("sassless", function(cb) {

    runSequence(['sass', 'less'], 'cssprocess', cb)

});


gulp.task("sass", function() {
    return gulp.src('Theme/scss/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('Theme/scss/'));

});


gulp.task("less", function() {
    return gulp.src('Theme/less/main.less')
    .pipe(less())
    .pipe(gulp.dest('Theme/less/'));

});


gulp.task('cssprocess', function () {
  return gulp
    .src(['Theme/scss/main.css','Theme/less/main.css'])
    .pipe(plumber())
    .pipe(concat("main.css"))
    .pipe(combineMq({
        beautify: false
    }))
    .pipe(autoprefixer({ browsers: ['last 2 versions','ie >= 9','iOS >= 8'], cascade: false }))
    .pipe(cssmin())
    .pipe(bless())
    .pipe(gulp.dest('Theme/css'))
    .pipe(connect.reload())
    .pipe(plumber.stop())
});


gulp.task('parker', function () {
  return gulp
    .src('Theme/css/main.css')
    .pipe(parker({
          metrics: [
            "TotalStylesheets",
            "TotalStylesheetSize",
            "TotalRules",
            "TotalSelectors",
            "TotalIdentifiers",
            "TotalDeclarations",
            "SelectorsPerRule",
            "IdentifiersPerSelector",
            "SpecificityPerSelector",
            "TopSelectorSpecificity",
            "TopSelectorSpecificitySelector",
            "TotalIdSelectors",
            "TotalUniqueColours",
            // "UniqueColours",
            "TotalImportantKeywords",
            "TotalMediaQueries",
            // "MediaQueries"

          ]
        }))
  });



gulp.task('kit', function () {
  return gulp
    .src('flats/views/*.kit')
    .pipe(plumber())
    .pipe(kit())
    .pipe(gulp.dest('flats'))
    .pipe(plumber.stop())
    .pipe(connect.reload());
  });

gulp.task('js', function () {
  return gulp
    .src([


                      'Theme/js/app.js',
                      'Theme/js/plugins.js',


                        // 'Theme/js/vendor/jquery.tubular.js',

                        'Theme/js/vendor/magnificpopup.js',

                        'Theme/js/vendor/jquery.unveil.js',
                        // 'Theme/js/vendor/jquery.fill.js',
                        'Theme/js/vendor/jquery.fittext.js',
                        'Theme/js/vendor/jquery.balancetext.js',
                        'Theme/js/vendor/jquery.waitforimages.js',
                        // 'Theme/js/vendor/magnificpopup.js',
                        'Theme/js/vendor/fastclick.js',


                        'Theme/js/vendor/pickadate/picker.js',
                        'Theme/js/vendor/pickadate/picker.date.js',



                        // 'Theme/js/vendor/jquery.pjax.js',

                        'Theme/js/vendor/jquery.kinetic.js',

                        'Theme/js/vendor/jquery.smoothwheel.js',



                        'Theme/js/vendor/slick/slick.js',


                        'Theme/js/vendor/jquery-validation/jquery.validate.js',

                        'Theme/js/vendor/jquery-validation/jquery.validation.net.webforms.js',



                        'Theme/js/vendor/debouncedresize.js',


                        'Theme/js/vendor/waypoints.js',

                        'Theme/js/vendor/waypoints-sticky.js',


                        // 'Theme/js/vendor/placeholder.js',

                        'Theme/js/vendor/selectordie.js',

                        'Theme/js/vendor/jquery.openclose.js',


                        // 'Theme/js/vendor/jquery.tweetHighlighted.js',
                        'Theme/js/vendor/jquery.creditCardValidator.js',


                        // 'Theme/js/vendor/jquery.cookie.js',

                        // 'Theme/js/vendor/waitForWebfonts.js',


                        'Theme/js/vendor/jquery.sticky-kit.js',

                        'Theme/js/vendor/jquery.transform2d.js',



                        'Theme/js/vendor/typeahead/typeahead.bundle.min.js',




                        'Theme/js/components/*.js',

                        'Theme/js/main.js'


                  ])
    .pipe(plumber())
    .pipe(concat("combined.js"))
    .pipe(uglify())
    .pipe(gulp.dest('Theme/js/'))
    .pipe(connect.reload())
    .pipe(plumber.stop())
    ;
});


gulp.task('watch', function () {

  gulp.watch(['Theme/scss/**/*.scss','Theme/less/**/*.less'], ["sassless"]);
  gulp.watch(['Flats/**/*.kit'], [ "kit" ]);
  gulp.watch(['Theme/js/**/*.js','!Theme/js/combined.js'], ["js"]);

});




gulp.task('default', ["connect","watch"]);

// gulp.task('compile', "compiles .kit,.scss, .less & .js files");



